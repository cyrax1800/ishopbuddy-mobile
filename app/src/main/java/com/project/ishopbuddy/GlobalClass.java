package com.project.ishopbuddy;

import com.google.gson.Gson;
import com.google.gson.JsonNull;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.Primitives;

import java.lang.reflect.Type;

/**
 * Created by michael on 12/12/16.
 */

public class GlobalClass {

    public static Gson gson = new Gson();

    public static <T> T getObjectFromJson(String json, Class<T> classOfT)throws JsonSyntaxException {
        Object object = gson.fromJson(json, (Type) classOfT);
        return Primitives.wrap(classOfT).cast(object);
    }

    public static String JSONtoString(Object src) {
        if (src == null) {
            return gson.toJson(JsonNull.INSTANCE);
        }
        return gson.toJson(src, src.getClass());
    }
}
