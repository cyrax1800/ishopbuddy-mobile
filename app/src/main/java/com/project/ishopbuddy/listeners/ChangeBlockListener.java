package com.project.ishopbuddy.listeners;

/**
 * Created by Michael on 12/23/2016.
 */

public interface ChangeBlockListener {
    public void changeBlock(Boolean block, int userId);
    public void removeBlock(int userId);
}
