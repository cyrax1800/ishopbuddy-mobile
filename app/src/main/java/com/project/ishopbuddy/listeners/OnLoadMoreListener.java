package com.project.ishopbuddy.listeners;

/**
 * Created by michael on 17/12/16.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
