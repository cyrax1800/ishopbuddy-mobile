package com.project.ishopbuddy.utils;

import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by michael on 20/12/16.
 */

public class Migration  implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        Log.d("tmp", "migrateInside: " + oldVersion);
        if (oldVersion == -1) {
            RealmObjectSchema categorySchema = schema.create("Category")
                    .addField("id", int.class, FieldAttribute.PRIMARY_KEY, FieldAttribute.INDEXED)
                    .addField("name", String.class, FieldAttribute.REQUIRED);

            oldVersion++;
        }
    }
}
