package com.project.ishopbuddy.utils;

import android.util.Log;

import com.project.ishopbuddy.model.Category;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by michael on 20/12/16.
 */
public class InitialData implements Realm.Transaction {
    @Override
    public void execute(Realm realm) {
        List<Category> categories = new ArrayList<>();
        Category category = realm.createObject(Category.class,1);
        category.setName("Produk Lainnya");
        categories.add(category);
        category = realm.createObject(Category.class,2);
        category.setName("Makanan Cepat Saji");
        categories.add(category);
        category = realm.createObject(Category.class,3);
        category.setName("Susu");
        categories.add(category);
        category = realm.createObject(Category.class,4);
        category.setName("Sabun");
        categories.add(category);
        Log.d("tmp", "execute: " + categories.toString());
        realm.insert(categories);
    }
}
