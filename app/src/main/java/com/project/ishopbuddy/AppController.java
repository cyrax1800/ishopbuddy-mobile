package com.project.ishopbuddy;

import android.app.Application;

import com.project.ishopbuddy.utils.InitialData;
import com.project.ishopbuddy.utils.Migration;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by mchen on 10/3/2016.
 */
public class AppController extends Application {

    public static final String DATABASE_NAME = "ishopbud";

    private static AppController mInstance;
    private static Realm realm;

    public void onCreate() {
        super.onCreate();

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(DATABASE_NAME)
                .schemaVersion(0)
                .migration(new Migration())
                .initialData(new InitialData())
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
//        getRealm().close();
//        Realm.deleteRealm(realmConfiguration);

        mInstance = this;
    }

    public static synchronized Realm getRealm() {
        return Realm.getDefaultInstance();
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }
}
