package com.project.ishopbuddy.model;

/**
 * Created by mchen on 10/11/2016.
 */

public class ReqToken {
    public String grant_type;
    public int client_id;
    public String client_secret;
    public String username;
    public String password;

    public ReqToken(String grant_type, int client_id, String client_secret, String username, String password){
        this.grant_type = grant_type;
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.username = username;
        this.password = password;
    }
}
