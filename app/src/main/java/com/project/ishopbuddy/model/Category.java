package com.project.ishopbuddy.model;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by michael on 20/12/16.
 */

public class Category extends RealmObject {

    public static String ID = "id";
    public static String NAME = "name";

    private static int Increment = -1;

    @PrimaryKey
    private int id;
    @Required
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getIncrement(Realm realm) {
        if(Increment == -1){
            if(realm.where(Category.class).findAll().size() == 0)
                Increment = 1;
            else
                Increment = realm.where(Category.class).max("id").intValue();
        }
        Increment += 1;
        return Increment;
    }
}
