package com.project.ishopbuddy.model;

import android.util.Log;

import java.util.Date;
import java.util.List;

/**
 * Created by mchen on 11/18/2016.
 */

public class Product extends BaseRespond {
    public int id;
    public String name;
    public List<Review> review;
    public List<Review> myreview;
    public String barcode;
    public String picture;
    public String created_at;
    public int total_review;
    public float rating;
    public User user;
    public List<Integer> summary_rating;
    public Category category;

    public Product(){

    }

    public String getUrlPic(int width, int height){
        return "http://manumeda.id/images/"+ width + "/" + height + "/" + picture;
    }

    public String getUrlPic(){
        return "http://manumeda.id/images/60/60/" + picture;
    }

    public String getTempUrlPic(){
        return "http://manumeda.id/images/120/120/" + picture.substring(picture.lastIndexOf("/") + 1);
    }

    @Override
    public String toString() {
        return "{" + "name : " + name+", picture:" +picture+"}";
    }
}
