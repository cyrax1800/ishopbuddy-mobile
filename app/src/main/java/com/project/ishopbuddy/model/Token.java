package com.project.ishopbuddy.model;

import java.util.Date;

/**
 * Created by mchen on 10/11/2016.
 */

public class Token extends BaseRespond {

    public String token_type;
    public long expires_in;
    public String access_token;
    public String refresh_token;
    public String hint;

    public Token(){

    }
}
