package com.project.ishopbuddy.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michael on 12/12/16.
 */

public class Search extends BaseRespond {
//    public List<Object> arrayList;
    @SerializedName("user")
    public List<User> user;
    @SerializedName("product")
    public List<Product> product;
}
