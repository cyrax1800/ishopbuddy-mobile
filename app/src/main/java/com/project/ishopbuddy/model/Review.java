package com.project.ishopbuddy.model;

/**
 * Created by mchen on 11/26/2016.
 */

public class Review {
    public int id;
    public int user_id;
    public int product_id;
    public float rating;
    public String review;
    public User user;

}
