package com.project.ishopbuddy.model;

/**
 * Created by mchen on 10/10/2016.
 */

public class User {

    public int id;
    public String username;
    public String name;
    public String email;
    public String profile_picture;
    public String token;
    public String bio;
    public String fb_id;
    public int total_followers;
    public int total_following;
    public boolean is_following;

    public User(){

    }

    public String getUrlPic(int width, int height){
        return "http://manumeda.id/images/"+ width + "/" + height + "/" + profile_picture;
    }

    public String getUrlPic(){
        return "http://manumeda.id/images/60/60/" + profile_picture;
    }

    public String getTempUrlPic(){
        return "http://manumeda.id/images/60/60/" + profile_picture.substring(profile_picture.lastIndexOf("/") + 1);
    }
}
