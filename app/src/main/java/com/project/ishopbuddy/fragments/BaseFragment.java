package com.project.ishopbuddy.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.project.ishopbuddy.managers.FragmentNavigator;

import io.realm.Realm;

/**
 * Created by MICHAEL on 9/6/2016.
 */
public class BaseFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "tmp";

    FragmentNavigator.FragmentNavigation mFragmentNavigation;

    protected void initView(View v){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNavigator.FragmentNavigation) {
            mFragmentNavigation = (FragmentNavigator.FragmentNavigation) context;
        }
    }

    @Override
    public void onClick(View v) {

    }
}
