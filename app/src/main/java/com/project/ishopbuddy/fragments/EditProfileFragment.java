package com.project.ishopbuddy.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.GlobalClass;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.model.User;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 11/27/2016.
 */

public class EditProfileFragment extends BaseFragment {
    public final static int REQUEST_IMAGE_CAPTURE = 2;

    SimpleDraweeView ivProfile;
    AppCompatButton btnUpload, btnDelete, btnSave, btnCancel;
    EditText etUsername, etDisplayName, etBio;
    ProgressDialog progressDialog;
    UserFragment.UpdateProfileInterface updateProfileInterface;

    User user;

    Uri photoUri = null;

    public boolean hasPicture;
    String mCurrentPhotoPath;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = GlobalClass.getObjectFromJson(getArguments().getString("user"),User.class);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        updateProfileInterface = (UserFragment.UpdateProfileInterface) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_profil,container,false);

        initView(v);

        btnUpload.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        return v;
    }

    @Override
    protected void initView(View v) {
        ivProfile = (SimpleDraweeView)v.findViewById(R.id.editprofile_iv_profile);
        btnUpload = (AppCompatButton)v.findViewById(R.id.editprofile_btn_upload_image);
        btnDelete = (AppCompatButton)v.findViewById(R.id.editprofile_btn_delete_image);
        btnSave = (AppCompatButton)v.findViewById(R.id.editprofile_btn_save);
        btnCancel = (AppCompatButton)v.findViewById(R.id.editpassword_btn_cancel);
        etUsername = (EditText)v.findViewById(R.id.editprofile_et_username);
        etDisplayName = (EditText)v.findViewById(R.id.editprofile_et_display_name);
        etBio = (EditText)v.findViewById(R.id.editprofile_et_bio);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Update Profile...");

        updateUI();

        super.initView(v);
    }

    private void updateUI() {
        if(user.profile_picture != null){
            Uri photoUri = Uri.parse(user.getTempUrlPic());
            ivProfile.setImageURI(photoUri);
        }
        etDisplayName.setText(user.name);
        etBio.setText(user.bio);
        etUsername.setText(user.username);
    }

    private void updateProfile() {
        progressDialog.show();
        MultipartBody.Part body;
        if(hasPicture){
            File tmpfile = new File(mCurrentPhotoPath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), tmpfile);
            body = MultipartBody.Part.createFormData("image", tmpfile.getName(), requestFile);
        }else{
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            body = MultipartBody.Part.createFormData("image", "", requestFile);
        }
        RequestBody userName = RequestBody.create(MediaType.parse("multipart/form-data"), etUsername.getText().toString());
        RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), etDisplayName.getText().toString());
        RequestBody bio = RequestBody.create(MediaType.parse("multipart/form-data"), etBio.getText().toString());
        Call<User> updateUser = APIManager.getUserRepo().updateProfile(APIManager.getTokenReqeust(),userName,name,bio,body);
        updateUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                progressDialog.dismiss();
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    User user = response.body();
                    updateProfileInterface.onUpdateProfile(user);
                    SessionManager.getInstance().save(SessionManager.USER,GlobalClass.JSONtoString(user));
                    getActivity().onBackPressed();
                    Log.d(TAG, "onResponse: " + user.toString());
                }else if(response.code() == 201){
                    Log.d(TAG, "onResponse: 201 ");
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            setPic();
        }
    }

    private void setPic() {
        hasPicture = true;
        int targetW = ivProfile.getWidth();
        int targetH = ivProfile.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor << 1;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        Uri uri = Uri.fromFile(new File(mCurrentPhotoPath));
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions(targetW,targetH))
                .build();

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(ivProfile.getController())
                .setImageRequest(request)
                .build();

        ivProfile.setController(controller);
        Log.d(TAG, "setPic: ");
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.i(TAG, "photo path = " + mCurrentPhotoPath);
        return image;
    }

    public static EditProfileFragment newInstance(User user) {
        Bundle args = new Bundle();
        args.putString("user", GlobalClass.JSONtoString(user));
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editprofile_btn_delete_image:
                hasPicture = false;
                photoUri = new Uri.Builder()
                        .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                        .path(String.valueOf(R.drawable.no_image_available))
                        .build();
                ivProfile.setImageURI(photoUri);
                break;
            case R.id.editprofile_btn_upload_image:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                    }
                    if (photoFile != null) {
                        photoUri = Uri.fromFile(photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                        getActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
                break;
            case R.id.editprofile_btn_save:
                updateProfile();
                break;
            case R.id.editprofile_btn_cancel:
                getActivity().onBackPressed();
                break;
        }

    }
}
