package com.project.ishopbuddy.fragments;

import android.content.DialogInterface;
import android.media.Rating;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.listeners.ClickListener;
import com.project.ishopbuddy.listeners.RecyclerTouchListener;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.model.Review;
import com.project.ishopbuddy.view.activities.MainActivity;
import com.project.ishopbuddy.view.adapters.ReviewAdapter;
import com.project.ishopbuddy.view.adapters.SmallProductAdapter;
import com.project.ishopbuddy.view.widgets.AddReviewDialogFragment;
import com.project.ishopbuddy.view.widgets.DividerItemDecoration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 11/27/2016.
 */

public class ProductDetailFragment extends BaseFragment implements PopupMenu.OnMenuItemClickListener {

    SimpleDraweeView ivProduct, ivOwnImage;
    TextView tvRate1,tvRate2,tvRate3,tvRate4,tvRate5, tvOwnUserName, tvOwnUserReview, tvOwnUserDate;
    AppCompatButton btnWriteReview, btnEditReview;
    RecyclerView recyclerView,rvRecomendationProduct;
    RatingBar ratbarOwn;
//    ImageButton ibMenu;
//    PopupMenu popupMenu;
//    MenuPopupHelper menuPopupHelper;
    LinearLayout llOwnReview;

//    AlertDialog deleteReviewDialog;
    AddReviewDialogFragment addReviewDialogFragment;
    ProgressBar progressBar;

    ReviewAdapter reviewAdapter;
    List<Review> reviews = new ArrayList<>();
    Review ownReview;

    SmallProductAdapter smallProductAdapter;
    List<Product> products = new ArrayList<>();

    private Product product;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_detail,container,false);

        product = new Gson().fromJson(getArguments().getString("product"),Product.class);

        initView(v);

        reviewAdapter = new ReviewAdapter(reviews);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(reviewAdapter);
        recyclerView.setNestedScrollingEnabled(false);
//        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                Log.d(TAG, "onClick: " + position);
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//                Log.d(TAG, "onClick: " + position);
//            }
//        }));

        smallProductAdapter = new SmallProductAdapter(products);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        rvRecomendationProduct.setLayoutManager(linearLayoutManager);
        rvRecomendationProduct.setAdapter(smallProductAdapter);
        rvRecomendationProduct.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.d(TAG, "onClick: " + position);
                mFragmentNavigation.pushFragment(ProductDetailFragment.newInstance(products.get(position)));
            }

            @Override
            public void onLongClick(View view, int position) {
                Log.d(TAG, "onClick: " + position);
            }
        }));


//        popupMenu.setOnMenuItemClickListener(this);
        btnWriteReview.setOnClickListener(this);
        btnEditReview.setOnClickListener(this);
//        ibMenu.setOnClickListener(this);

        getDetailProduct();
        getProductReview();
        getRecomendation();

        return v;
    }

    private void getRecomendation() {
        Call<List<Product>> recomendationCall = APIManager.getProductRepo().getProductByCategory(APIManager.getTokenReqeust(),product.category.getId());
        recomendationCall.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    List<Product> tmpProducts = response.body();
                    if(tmpProducts.size() != 0){
                        rvRecomendationProduct.setVisibility(View.VISIBLE);
                        products.addAll(tmpProducts);
                        smallProductAdapter.notifyDataSetChanged();
                    }else{
                        rvRecomendationProduct.setVisibility(View.GONE);
                    }
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        if(resError.code == 404){
                        }
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public void getDetailProduct(){
        Call<Product> productCall = APIManager.getProductRepo().getProductByBarcode(APIManager.getTokenReqeust(),product.barcode);
        productCall.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    product = response.body();
                    if(product.myreview.size() != 0){
                        setOwnReviewView(product.myreview.get(0));
                    }else {
                        llOwnReview.setVisibility(View.GONE);
                        btnWriteReview.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                    Log.d(TAG, "onResponse: " + product);
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        if(resError.code == 404){
                        }
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public void getProductReview(){
        Call<List<Review>> allReview = APIManager.getReviewRepo().getReviewProductFrom(APIManager.getTokenReqeust(),product.id);
        allReview.enqueue(new Callback<List<Review>>() {
            @Override
            public void onResponse(Call<List<Review>> call, Response<List<Review>> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    List<Review> tmpReviews = response.body();
                    reviews.clear();
                    reviews.addAll(tmpReviews);
                    reviewAdapter.notifyDataSetChanged();
                    Log.d(TAG, "onResponse: " + reviewAdapter.getItemCount());
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Review>> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    @Override
    protected void initView(View v) {
        ivProduct = (SimpleDraweeView)v.findViewById(R.id.cover_image);
        ivOwnImage = (SimpleDraweeView)v.findViewById(R.id.own_sdv_profile_picture);
        tvRate1 = (TextView) v.findViewById(R.id.tv_rate_1);
        tvRate2 = (TextView) v.findViewById(R.id.tv_rate_2);
        tvRate3 = (TextView) v.findViewById(R.id.tv_rate_3);
        tvRate4 = (TextView) v.findViewById(R.id.tv_rate_4);
        tvRate5 = (TextView) v.findViewById(R.id.tv_rate_5);
        tvOwnUserName = (TextView) v.findViewById(R.id.own_tv_username);
        tvOwnUserReview = (TextView) v.findViewById(R.id.own_tv_review);
        tvOwnUserDate = (TextView) v.findViewById(R.id.own_tv_tanggal);
        btnWriteReview = (AppCompatButton)v.findViewById(R.id.btn_product_detail_write_review);
        btnEditReview = (AppCompatButton)v.findViewById(R.id.own_btn_edit);
        recyclerView = (RecyclerView) v.findViewById(R.id.rv_product_detail);
        ratbarOwn = (RatingBar) v.findViewById(R.id.own_ratbar);
//        ibMenu = (ImageButton) v.findViewById(R.id.own_ib_menu);
        llOwnReview = (LinearLayout) v.findViewById(R.id.own_container);
        progressBar = (ProgressBar)v.findViewById(R.id.progress_bar);
        rvRecomendationProduct = (RecyclerView) v.findViewById(R.id.rv_recomendation);

        if(product.picture != null){
            Uri imageUri = Uri.parse(product.getTempUrlPic());
            ivProduct.setImageURI(imageUri);
        }

        llOwnReview.setVisibility(View.GONE);
        btnWriteReview.setVisibility(View.GONE);

        addReviewDialogFragment = new AddReviewDialogFragment();
        addReviewDialogFragment.setOnInputReviewSuccess(new InputReviewCallback() {
            @Override
            public void addReveiewSuccess(Review review) {
                setOwnReviewView(review);
                getProductReview();
            }
        });

        MainActivity.getTitleToolBar().setVisibility(View.VISIBLE);
        MainActivity.getTitleToolBar().setText(product.name);

//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(R.string.delete)
//                .setMessage("Are you sure want to delete your review of this product?")
//                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        deleteReview();
//                    }
//                })
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        deleteReviewDialog.dismiss();
//                    }
//                });

//        deleteReviewDialog = builder.create();

        if(product.summary_rating != null){
            tvRate1.setText(String.valueOf(product.summary_rating.get(0)));
            tvRate2.setText(String.valueOf(product.summary_rating.get(1)));
            tvRate3.setText(String.valueOf(product.summary_rating.get(2)));
            tvRate4.setText(String.valueOf(product.summary_rating.get(3)));
            tvRate5.setText(String.valueOf(product.summary_rating.get(4)));
        }

//        popupMenu = new PopupMenu(getContext(),ibMenu);
//        popupMenu.inflate(R.menu.review_menu);
//        menuPopupHelper = new MenuPopupHelper(getContext(),(MenuBuilder)popupMenu.getMenu(),ibMenu);
//        menuPopupHelper.setForceShowIcon(true);

        super.initView(v);
    }

    private void deleteReview() {
        Call<Review> deleteReview = APIManager.getReviewRepo().deleteReview(APIManager.getTokenReqeust(),ownReview.id);
        deleteReview.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(Call<Review> call, Response<Review> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 204){
                    Log.d(TAG, "onResponse: ");
                    ownReview = null;

                    llOwnReview.setVisibility(View.GONE);
                    btnWriteReview.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    getProductReview();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public void setOwnReviewView(Review review){
        // Disini lakukan update view untuk review sndri
        llOwnReview.setVisibility(View.VISIBLE);
        btnWriteReview.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        ownReview = review;
        if(review.user != null){
            if(review.user.profile_picture != null){
                Uri profileImageUri = Uri.parse(review.user.getTempUrlPic());
                ivOwnImage.setImageURI(profileImageUri);
            }
            tvOwnUserName.setText(review.user.name);
        }
        tvOwnUserReview.setText(review.review);
        ratbarOwn.setRating(review.rating);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            Log.d(TAG, "onHiddenChanged: ");
            MainActivity.getTitleToolBar().setVisibility(View.VISIBLE);
            MainActivity.getTitleToolBar().setText(product.name);
        }else{
            MainActivity.getTitleToolBar().setVisibility(View.VISIBLE);
            MainActivity.getTitleToolBar().setText(R.string.app_name);
        }
    }

    public static ProductDetailFragment newInstance(Product product) {
        Bundle args = new Bundle();
        args.putString("product",new Gson().toJson(product));
        ProductDetailFragment fragment = new ProductDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_product_detail_write_review:
                addReviewDialogFragment.show(getFragmentManager(), product.id, "addReviewDialogFragment");
                break;
            case R.id.own_btn_edit:
//                menuPopupHelper.show();
                addReviewDialogFragment.show(getFragmentManager(), product.id, ownReview, "OwnReviewDialogFragment");
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_delete_review:
                return true;
            case R.id.menu_modify_review:
//                addReviewDialogFragment.show(getFragmentManager(), product.id, ownReview, "OwnReviewDialogFragment");
                return true;
        }
        return false;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public interface InputReviewCallback{
        public void addReveiewSuccess(Review review);
    }
}
