package com.project.ishopbuddy.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.managers.FragmentNavigator;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Category;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.view.activities.MainActivity;
import com.project.ishopbuddy.view.adapters.CategoryAdapter;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 11/26/2016.
 */

public class AddProductFragment extends BaseFragment {
    public static final int REQUEST_IMAGE_CAPTURE = 1;

    TextView tvBarcodeCode, tvCategory;
    EditText etItemName,etReview;
    RatingBar ratingBar;
    ImageView ivImageProduct;
    AppCompatButton btnUpload,btnDone,btnCancel;
    ProgressDialog progressDialog;
    AlertDialog categoryDialog;

    CategoryAdapter categoryAdapter;
    List<Category> categories = new ArrayList<>();

    Realm realm;

    Uri photoUri = null;
    public boolean hasPicture;
    String mCurrentPhotoPath;
    String barcode;
    float ratingValue;
    int categoryId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_product,container,false);

        barcode = getArguments().getString("barcode");
        hasPicture = false;
        categoryId = -1;

        categories = realm.where(Category.class).findAll();
        categoryAdapter = new CategoryAdapter(getContext(),categories);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.select_category)
                .setAdapter(categoryAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if((i < categories.size()) && (i > -1)) {
                            tvCategory.setText(categories.get(i).getName());
                            categoryId = categories.get(i).getId();
                        }
                    }
                });

        categoryDialog = builder.create();
        categoryDialog.getListView().setDivider(new ColorDrawable(Color.BLUE));
        categoryDialog.getListView().setDividerHeight(2);

        initView(v);

        ratingValue = ratingBar.getRating();

        btnUpload.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        tvCategory.setOnClickListener(this);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ratingValue = v;
            }
        });

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Posting product...");

        return v;
    }

    @Override
    protected void initView(View v) {
        tvBarcodeCode = (TextView)v.findViewById(R.id.tv_frag_add_code);
        tvCategory = (TextView)v.findViewById(R.id.tv_category);
        etItemName = (EditText) v.findViewById(R.id.et_frag_add_item_name);
        etReview = (EditText) v.findViewById(R.id.et_frag_add_review);
        ratingBar = (RatingBar) v.findViewById(R.id.ratstar_frag_add);
        ivImageProduct = (ImageView) v.findViewById(R.id.iv_frag_add_image);
        btnUpload = (AppCompatButton) v.findViewById(R.id.btn_frag_add_upload_image);
        btnDone = (AppCompatButton) v.findViewById(R.id.btn_frag_add_done);
        btnCancel = (AppCompatButton) v.findViewById(R.id.btn_frag_add_cancel);

        tvBarcodeCode.setText("Barcode: " + barcode);
    }

    public static AddProductFragment newInstance(String barcode) {
        Bundle args = new Bundle();
        args.putString("barcode",barcode);
        AddProductFragment fragment = new AddProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            setPic();
        }
    }

    private void setPic() {
        hasPicture = true;
        int targetW = ivImageProduct.getWidth();
        int targetH = ivImageProduct.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor << 1;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        ivImageProduct.setImageBitmap(bitmap);
        Log.d(TAG, "setPic: ");
    }

    private boolean validate(){
        if(categoryId == -1) return false;
        if(ratingValue == 0) return false;
        if(etItemName.getText().toString().equals("")) return false;
        if(etReview.getText().toString().equals("")) return false;
        return true;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.i(TAG, "photo path = " + mCurrentPhotoPath);
        return image;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_category:
                categoryDialog.show();
                break;
            case R.id.btn_frag_add_upload_image:
                if(hasPicture) return;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                    }
                    if (photoFile != null) {
                        photoUri = Uri.fromFile(photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                        getActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
                break;
            case R.id.btn_frag_add_cancel:
                MainActivity.getInstance().onBackPressed();
                break;
            case R.id.btn_frag_add_done:
                if(!validate()){
                    Toast.makeText(getContext(),"All input must be fill",Toast.LENGTH_SHORT).show();
                    return;
                }
                progressDialog.show();
                MultipartBody.Part body;
                if(hasPicture){
                    File tmpfile = new File(mCurrentPhotoPath);
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), tmpfile);
                    body = MultipartBody.Part.createFormData("image", tmpfile.getName(), requestFile);
                }else{
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                    body = MultipartBody.Part.createFormData("image", "", requestFile);
                }
                RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), etItemName.getText().toString());
                RequestBody rbBarcode = RequestBody.create(MediaType.parse("multipart/form-data"), barcode);
                RequestBody review = RequestBody.create(MediaType.parse("multipart/form-data"), etReview.getText().toString());
                Call<Product> upload = APIManager.getProductRepo().addProduct(APIManager.getTokenReqeust(),name,rbBarcode,ratingValue,hasPicture,review,categoryId,body);
                upload.enqueue(new Callback<Product>() {
                    @Override
                    public void onResponse(Call<Product> call, Response<Product> response) {
                        progressDialog.dismiss();
                        Log.d(TAG, "onResponse: Call With Code " + response.code());
                        Log.d(TAG, "onResponse: Call Message " + response.message());
                        if(response.code() == 201){
                            Product product = response.body();
                            Log.d(TAG, "onResponse: " + product.toString());
                            ((MainActivity)getActivity()).fragManager.clearStack(MainActivity.SCAN);
                            getActivity().onBackPressed();
                        }else{
                            Gson gson = new GsonBuilder().create();
                            try {
                                BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                                Log.d(TAG, "onResponse: " + resError.message);
                            } catch (IOException e) {
                                Log.d(TAG, "onResponse: " + "error");
                            }
                            Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Product> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(),"An error has occured, please try again",Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onFailure: Call Fail " + t.toString());
                    }
                });
                break;
        }
    }
}
