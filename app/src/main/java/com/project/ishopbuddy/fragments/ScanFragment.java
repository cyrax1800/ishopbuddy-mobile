package com.project.ishopbuddy.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.text.Text;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.managers.FragmentNavigator;
import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.view.activities.BarcodeCaptureActivity;
import com.project.ishopbuddy.view.activities.MainActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 10/16/2016.
 */

public class ScanFragment extends BaseFragment {

    public static final int SCAN = 0;

    TextView tvCode,tvAccount,tvItemName,tvAllReview,tvReviewValue, tvRatingUserCount;
    RatingBar ratingBar;
    SimpleDraweeView sdvProductPhoto;
    AppCompatButton btnScan,btnAdd;
    LinearLayout llFound,llNotFound;
    ProgressDialog progressDialog;

    String barcodeValue;
    Product product;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_scan,container,false);
        initView(v);
        btnScan.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        tvAllReview.setOnClickListener(this);

        return v;
    }

    @Override
    protected void initView(View v) {
        sdvProductPhoto = (SimpleDraweeView) v.findViewById(R.id.iv_image_product);
        tvItemName = (TextView) v.findViewById(R.id.hol_product_name);
        tvAccount = (TextView) v.findViewById(R.id.hol_product_user);
        tvReviewValue = (TextView) v.findViewById(R.id.hol_product_rating_text);
        tvRatingUserCount = (TextView) v.findViewById(R.id.hol_product_user_count);
        ratingBar = (RatingBar) v.findViewById(R.id.hol_product_rating_bar);
        tvAllReview = (TextView) v.findViewById(R.id.tv_all_review);
        tvCode = (TextView) v.findViewById(R.id.tv_barcode);
        btnScan = (AppCompatButton) v.findViewById(R.id.scan_again);
        btnAdd = (AppCompatButton) v.findViewById(R.id.btn_add);
        llFound = (LinearLayout) v.findViewById(R.id.container_found);
        llNotFound = (LinearLayout) v.findViewById(R.id.container_not_found);
        llFound.setVisibility(View.GONE);
        llNotFound.setVisibility(View.GONE);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Searching product...");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SCAN){
            if(resultCode == CommonStatusCodes.SUCCESS){
                Log.d(TAG, "onActivityResult: value: " + data.getStringExtra("value"));
                Log.d(TAG, "onActivityResult: type: " + data.getStringExtra("type"));
                barcodeValue = data.getStringExtra("value");
                progressDialog.show();
                getProductByBarcode(barcodeValue);
            }else if(resultCode == CommonStatusCodes.CANCELED){
                Log.d(TAG, "onActivityResult: value: " + data.getStringExtra("value"));
                Log.d(TAG, "onActivityResult: type: " + data.getStringExtra("type"));
                Log.d(TAG, "onActivityResult: Cancelled ");
                MainActivity.getInstance().fragManager.switchTabWithoutPushTab(FragmentNavigator.TAB1);
                MainActivity.getInstance().mBottomBar.selectTabAtPosition(0,false);
            }
        }
    }

    public void getProductByBarcode(String barcode){
        Call<Product> getProduct = APIManager.getProductRepo().getProductByBarcode(APIManager.getTokenReqeust(),barcode);
        getProduct.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                progressDialog.dismiss();
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    product = response.body();
                    updateView(product);
                    Log.d(TAG, "onResponse: " + product);
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        if(resError.code == 404){
                            updateView(barcodeValue);
                        }
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
//                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public void updateView(Product product){
        if(product.picture != null){
            Uri imageUri = Uri.parse(product.getTempUrlPic());
            sdvProductPhoto.setImageURI(imageUri);
        }
        tvItemName.setText(product.name);
        tvAccount.setText("@" + product.user.name);
        tvReviewValue.setText(String.valueOf(product.rating));
        tvRatingUserCount.setText(String.valueOf(product.total_review));
        if(product.review != null && product.review.size() != 0)
            ratingBar.setRating(product.review.get(0).rating);
        llFound.setVisibility(View.VISIBLE);
    }

    public void updateView(String barcode){
        tvCode.setText(barcode);
        llNotFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            llFound.setVisibility(View.GONE);
            llNotFound.setVisibility(View.GONE);
        }
    }

    public static ScanFragment newInstance() {
        Bundle args = new Bundle();
        ScanFragment fragment = new ScanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan_again:
                llFound.setVisibility(View.GONE);
                llNotFound.setVisibility(View.GONE);
                Intent i = new Intent(getContext(),BarcodeCaptureActivity.class);
                getActivity().startActivityForResult(i,ScanFragment.SCAN);
                break;
            case R.id.btn_add:
                mFragmentNavigation.pushFragment(AddProductFragment.newInstance(barcodeValue));
                break;
            case R.id.tv_all_review:
                mFragmentNavigation.pushFragment(ProductDetailFragment.newInstance(product));
                break;
        }
    }
}
