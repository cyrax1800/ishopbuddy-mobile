package com.project.ishopbuddy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.project.ishopbuddy.GlobalClass;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Category;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.model.Search;
import com.project.ishopbuddy.model.User;
import com.project.ishopbuddy.view.activities.MainActivity;
import com.project.ishopbuddy.view.adapters.SearchPagerAdapter;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 10/16/2016.
 */

public class SearchFragment extends BaseFragment {

    ViewPager viewPager;
    TabLayout tabLayout;
    SearchPagerAdapter searchPagerAdapter;

    SearchView searchView;
    MenuItem mSearchMenuItem;
    ImageView mCloseButton;

    Call<List<Object>> searchCall;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search,container,false);
        initView(v);

        MainActivity.getToolBar().inflateMenu(R.menu.search_menu);
        MainActivity.getTitleToolBar().setVisibility(View.GONE);

        mSearchMenuItem = MainActivity.getToolBar().getMenu().findItem(R.id.action_search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setIconifiedByDefault(false);
        mCloseButton = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        mCloseButton.setVisibility(View.GONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mCloseButton.setVisibility(newText.isEmpty() ? View.GONE : View.VISIBLE);
                if(!newText.isEmpty()){
                    search(newText);
                }
                return false;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                Log.d(TAG, "onFocusChange: ");
                mCloseButton.setVisibility(View.GONE);
            }
        });

        return v;
    }

    private void search(String keyword) {
        if(searchCall != null && searchCall.isExecuted())
            searchCall.cancel();
        searchCall = APIManager.getSearchRepo().search(APIManager.getTokenReqeust(),keyword);
        searchCall.enqueue(new Callback<List<Object>>() {
            @Override
            public void onResponse(Call<List<Object>> call, Response<List<Object>> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    List<Object> objects = response.body();
                    Search search = new Search();
                    String tmpString = GlobalClass.JSONtoString(objects.get(1));
                    search.product = GlobalClass.getObjectFromJson(tmpString,Search.class).product;
                    tmpString = GlobalClass.JSONtoString(objects.get(0));
                    search.user = GlobalClass.getObjectFromJson(tmpString,Search.class).user;
//                    TypeToken<List<User>> token = new TypeToken<List<User>>() {};
//                    search = GlobalClass.gson.fromJson(objects.get(0).toString(), token.getType());
//                    search = GlobalClass.gson.fromJson(objects.get(1).toString(), Search.class);
//                    Log.d(TAG, "onResponse: " + objects.get(1));
//                    Log.d(TAG, "onResponse: " + search.product.size());
//                    Log.d(TAG, "onResponse: " + searchs.get(0).product);
                    if(search.product.size() != 0){
                        Log.d(TAG, "onResponse: " + search.product.size());
                        searchPagerAdapter.searchProductFragment.setResult(search.product);
                    }
                    if(search.user.size() != 0){
                        Log.d(TAG, "onResponse: " + search.user.size());
                        searchPagerAdapter.searchUserFragment.setResult(search.user);
                    }
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Object>> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    @Override
    protected void initView(View v) {
        searchPagerAdapter = new SearchPagerAdapter(getChildFragmentManager(),getContext());

        viewPager = (ViewPager)v.findViewById(R.id.vpPager);
        viewPager.setAdapter(searchPagerAdapter);

        tabLayout = (TabLayout)v.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager,true);
    }

    public static SearchFragment newInstance() {

        Bundle args = new Bundle();

        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            MainActivity.getToolBar().inflateMenu(R.menu.search_menu);
            MainActivity.getTitleToolBar().setVisibility(View.GONE);
            mSearchMenuItem = MainActivity.getToolBar().getMenu().findItem(R.id.action_search);
            searchView = (SearchView) mSearchMenuItem.getActionView();
            searchView.setMaxWidth(Integer.MAX_VALUE);
            searchView.setIconifiedByDefault(false);
            mCloseButton = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
            mCloseButton.setVisibility(View.GONE);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    mCloseButton.setVisibility(newText.isEmpty() ? View.GONE : View.VISIBLE);
                    if(!newText.isEmpty()){
                        search(newText);
                    }
                    return false;
                }
            });

            searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    Log.d(TAG, "onFocusChange: ");
                    mCloseButton.setVisibility(View.GONE);
                }
            });
        }else{
            Log.d(TAG, "onHiddenChanged: ");
            mCloseButton.setVisibility(View.VISIBLE);
            searchView.clearFocus();
            searchView.setIconified(true);
            searchView.setVisibility(View.GONE);
            MainActivity.getTitleToolBar().setVisibility(View.VISIBLE);
            MainActivity.getTitleToolBar().setText(R.string.app_name);
            MainActivity.getToolBar().getMenu().clear();
        }
    }
}
