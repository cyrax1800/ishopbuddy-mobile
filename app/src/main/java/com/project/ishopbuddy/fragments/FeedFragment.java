package com.project.ishopbuddy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.listeners.ClickListener;
import com.project.ishopbuddy.listeners.OnLoadMoreListener;
import com.project.ishopbuddy.listeners.RecyclerTouchListener;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.view.adapters.ProductAdapter;
import com.project.ishopbuddy.view.widgets.DividerItemDecoration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 10/16/2016.
 */

public class FeedFragment extends BaseFragment {

    RecyclerView recyclerView;
    TextView tvNoProduct;
    SwipeRefreshLayout mSwipeRefreshLayout;

    ProductAdapter productAdapter;

    public Boolean isEndofData;

    List<Product> productList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_feed,container,false);
        initView(v);

        isEndofData = false;

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        productAdapter = new ProductAdapter(productList);
        recyclerView.setAdapter(productAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mFragmentNavigation.pushFragment(ProductDetailFragment.newInstance(productList.get(position)));
                Log.d(TAG, "onClick: " + position);
            }

            @Override
            public void onLongClick(View view, int position) {
                Log.d(TAG, "onClick: " + position);
            }
        }));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
                productAdapter.totalItemCount = mLayoutManager.getItemCount();
                productAdapter.lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                if (!isEndofData && !productAdapter.isLoading && productAdapter.totalItemCount <= (productAdapter.lastVisibleItem + productAdapter.visibleThreshold)) {
                    if (productAdapter.getmOnLoadMoreListener() != null) {
                        productAdapter.getmOnLoadMoreListener().onLoadMore();
                    }
                    productAdapter.isLoading = true;
                }
            }
        });

        productAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                productList.add(new Product());
                productAdapter.notifyItemInserted(productList.size() - 1);
//                productAdapter.notifyDataSetChanged();
                Log.d(TAG, "Load More" + productAdapter._products.size());

                //Load more data for reyclerview
                getProductPage(productAdapter.page + 1);
            }
        });


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllProduct();
            }
        });

        getAllProduct();

        return v;
    }

    private void getProductPage(int page) {
        Call<List<Product>> productCall = APIManager.getProductRepo().getProductByPage(APIManager.getTokenReqeust(),page);
        productCall.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    productList.remove(productList.size() - 1);
                    productAdapter.notifyItemRemoved(productList.size());
                    List<Product> products = response.body();
//                    productList.addAll(product    );
                    productAdapter.page++;
//                    productAdapter.notifyDataSetChanged();

                    for (int i = 0 ;i< products.size() ; i++){
                        Log.d(TAG, "onResponse: " + i);
                        productList.add(products.get(i));
                        productAdapter.notifyItemInserted(productList.size() - 1);
                    }
                    productAdapter.setLoaded();
                    if(products.size() < 10){
                        isEndofData = true;
                    }
                    Log.d(TAG, "onResponse: " + productAdapter.getItemCount());
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public void updateUI(){
        tvNoProduct.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    public void getAllProduct(){
        Call<List<Product>> getProduct = APIManager.getProductRepo().getAllProduct(APIManager.getTokenReqeust());
        getProduct.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    List<Product> products = response.body();
                    productList.clear();
                    productList.addAll(products);
                    productAdapter.notifyDataSetChanged();
                    productAdapter.page = 1;
                    if(mSwipeRefreshLayout.isRefreshing()){
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    isEndofData = false;
                    updateUI();
                    Log.d(TAG, "onResponse: " + productAdapter.getItemCount());
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    @Override
    protected void initView(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        tvNoProduct = (TextView)v.findViewById(R.id.tv_no_product);
        mSwipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.srl_list_product);
    }

    public static FeedFragment newInstance() {

        Bundle args = new Bundle();

        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        /*switch (v.getId()) {

        }*/
    }
}
