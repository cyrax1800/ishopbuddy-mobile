package com.project.ishopbuddy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.ishopbuddy.R;
import com.project.ishopbuddy.listeners.ClickListener;
import com.project.ishopbuddy.listeners.RecyclerTouchListener;
import com.project.ishopbuddy.model.User;
import com.project.ishopbuddy.view.adapters.UserAdapter;
import com.project.ishopbuddy.view.widgets.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michael on 12/12/16.
 */

public class SearchUserFragment extends BaseFragment {

    RecyclerView recyclerView;

    UserAdapter userAdapter;

    List<User> userList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pager_fragment_search_user,container,false);
        initView(v);

        userAdapter = new UserAdapter(userList,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(userAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mFragmentNavigation.pushFragment(UserFragment.newInstance(false,userList.get(position)));
                Log.d(TAG, "onClick: " + position);
            }

            @Override
            public void onLongClick(View view, int position) {
                Log.d(TAG, "onClick: " + position);
            }
        }));

        return v;
    }

    public void setResult(List<User> users){
        userList.clear();
        userList.addAll(users);
        userAdapter.notifyDataSetChanged();
    }

    @Override
    protected void initView(View v) {
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
    }

    public static SearchUserFragment newInstance() {

        Bundle args = new Bundle();

        SearchUserFragment fragment = new SearchUserFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
