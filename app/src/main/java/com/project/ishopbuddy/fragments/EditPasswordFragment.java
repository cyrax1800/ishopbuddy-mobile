package com.project.ishopbuddy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.model.BaseRespond;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by michael on 05/12/16.
 */

public class EditPasswordFragment extends BaseFragment {

    AppCompatButton btnSave,btnCancel;
    EditText etOldPassword,etPassword,etRePassword;

    String oldPass,pass,repass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_change_password,container,false);
        initView(v);

        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        return v;
    }

    @Override
    protected void initView(View v) {
        btnSave = (AppCompatButton)v.findViewById(R.id.editpassword_btn_save);
        btnCancel = (AppCompatButton)v.findViewById(R.id.editpassword_btn_cancel);
        etOldPassword = (EditText)v.findViewById(R.id.editpassword_et_old_password);
        etPassword = (EditText)v.findViewById(R.id.editpassword_et_password);
        etRePassword = (EditText)v.findViewById(R.id.editpassword_et_re_password);
    }

    private void validate(){
        oldPass = etOldPassword.getEditableText().toString();
        pass = etPassword.getEditableText().toString();
        repass = etPassword.getEditableText().toString();
        if(oldPass.equals("") || pass.equals("") || repass.equals("")){
            Toast.makeText(getContext(),"Tidak ada yang boleh kosong",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!pass.equals(repass)){
            Toast.makeText(getContext(),"Password dan validasi password tidak sama",Toast.LENGTH_SHORT).show();
            return;
        }
        changePassword();
    }

    private void changePassword() {
        HashMap<String,String> map = new HashMap<>();
        map.put("current",oldPass);
        map.put("password",pass);
        map.put("password_confirmation",repass);
        Call<BaseRespond> changePass = APIManager.getUserRepo().changePassword(APIManager.getTokenReqeust(),map);
        changePass.enqueue(new Callback<BaseRespond>() {
            @Override
            public void onResponse(Call<BaseRespond> call, Response<BaseRespond> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    BaseRespond baseRespond = response.body();
                    Log.d(TAG, "onResponse: ");
                    getActivity().onBackPressed();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        if(resError.code == 404){

                        }else if(resError.code == 400){
                            Toast.makeText(getContext(),"Error" , Toast.LENGTH_SHORT).show();
                        }
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
//                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseRespond> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public static EditPasswordFragment newInstance() {

        Bundle args = new Bundle();

        EditPasswordFragment fragment = new EditPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editpassword_btn_save:
                validate();
                break;
            case R.id.editpassword_btn_cancel:
                getActivity().onBackPressed();
                break;
        }
    }
}
