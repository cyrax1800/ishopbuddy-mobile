package com.project.ishopbuddy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.listeners.ChangeBlockListener;
import com.project.ishopbuddy.listeners.ClickListener;
import com.project.ishopbuddy.listeners.RecyclerTouchListener;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.User;
import com.project.ishopbuddy.view.adapters.UserAdapter;
import com.project.ishopbuddy.view.widgets.DividerItemDecoration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by michael on 12/12/16.
 */

public class FollowersFragment extends BaseFragment {
    public final static int FOLLOWERS = 0;
    public final static int FOLLOWING = 1;

    RecyclerView recyclerView;

    UserAdapter userAdapter;

    List<User> userList = new ArrayList<>();

    private int type;
    private int userId;

    Call<User> followCall;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt("type");
        userId = getArguments().getInt("userId");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_follow_user,container,false);
        initView(v);

        userAdapter = new UserAdapter(userList,true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(userAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.d(TAG, "onClick: " + position);
            }

            @Override
            public void onLongClick(View view, int position) {
                Log.d(TAG, "onClick: " + position);
            }
        }));

        userAdapter.setChangeBlockListener(new ChangeBlockListener() {
            @Override
            public void changeBlock(Boolean block, int userId) {
                if(block){
                    follow(userId);
                }else{
                    unFollow(userId);
                }
            }

            @Override
            public void removeBlock(int userId) {

            }
        });

        if(type == FOLLOWERS){
            getFollowers();
        }else if(type == FOLLOWING){
            getFollowing();
        }

        return v;
    }

    private void follow(int userID){
        if(followCall != null && followCall.isExecuted()){
            followCall.cancel();
        }
        followCall = APIManager.getUserRepo().follow(APIManager.getTokenReqeust(),userID);
        followCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Log.d(TAG, "onResponse: ");
                    User tmpUser = response.body();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    private void unFollow(int userID){
        if(followCall != null && followCall.isExecuted()){
            followCall.cancel();
        }
        followCall = APIManager.getUserRepo().unFollow(APIManager.getTokenReqeust(),userID);
        followCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Log.d(TAG, "onResponse: ");
                    User tmpUser = response.body();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    private void getFollowers() {
        Call<List<User>> getFollowersCall = APIManager.getUserRepo().getFollowers(APIManager.getTokenReqeust());
        getFollowersCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Log.d(TAG, "onResponse: ");
                    List<User> users = response.body();
                    userList.clear();
                    userList.addAll(users);
                    userAdapter.notifyDataSetChanged();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    private void getFollowing(){
        Call<List<User>> getFollowingCall = APIManager.getUserRepo().getFollowing(APIManager.getTokenReqeust(),userId);
        getFollowingCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Log.d(TAG, "onResponse: ");
                    List<User> users = response.body();
                    userList.clear();
                    userList.addAll(users);
                    userAdapter.notifyDataSetChanged();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    @Override
    protected void initView(View v) {
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
    }

    public static FollowersFragment newInstance(int type, int userId) {
        Bundle args = new Bundle();
        args.putInt("type",type);
        args.putInt("userId",userId);
        FollowersFragment fragment = new FollowersFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
