package com.project.ishopbuddy.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.GlobalClass;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.managers.GoogleApiManager;
import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.User;
import com.project.ishopbuddy.view.activities.LoginActivity;
import com.project.ishopbuddy.view.activities.MainActivity;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 10/16/2016.
 */

public class UserFragment extends BaseFragment {

    SimpleDraweeView ivProfilePicture;
    TextView tvAccount, tvDisplayName, tvBio, tvFollower, tvFollowing;
    AppCompatButton btnEdit,btnLogout,btnEditPassword, btnFollow;
    LinearLayout llButton, llFollow;

    Call<User> followCall;
    User user;

    Boolean isFollow;
    Boolean isOwn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isOwn = getArguments().getBoolean("isOwn");

        if(isOwn){
            user = GlobalClass.getObjectFromJson(SessionManager.getInstance().getValueString(SessionManager.USER),User.class);
        }
        else{
            user = GlobalClass.getObjectFromJson(getArguments().getString("user"),User.class);
            isFollow = user.is_following;
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user,container,false);

        initView(v);
        btnEdit.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnEditPassword.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        tvFollowing.setOnClickListener(this);
        tvFollower.setOnClickListener(this);

        return v;
    }

    @Override
    protected void initView(View v) {
        ivProfilePicture = (SimpleDraweeView) v.findViewById(R.id.iv_profile_picture);
        tvAccount = (TextView) v.findViewById(R.id.tv_account_name);
        tvDisplayName = (TextView) v.findViewById(R.id.tv_display_name);
        tvBio = (TextView) v.findViewById(R.id.tv_bio);
        tvFollower = (TextView) v.findViewById(R.id.tv_follower);
        tvFollowing = (TextView) v.findViewById(R.id.tv_following);
        btnEdit = (AppCompatButton) v.findViewById(R.id.btn_edit_profile);
        btnLogout = (AppCompatButton) v.findViewById(R.id.btn_logout);
        btnEditPassword = (AppCompatButton) v.findViewById(R.id.btn_edit_password);
        btnFollow = (AppCompatButton) v.findViewById(R.id.btn_follow);
        llButton = (LinearLayout) v.findViewById(R.id.layout_button);
        llFollow = (LinearLayout) v.findViewById(R.id.ll_follow);

        if(isOwn){
            btnFollow.setVisibility(View.GONE);
            llButton.setVisibility(View.VISIBLE);
            if(!SessionManager.getInstance().getValueString(SessionManager.LOGIN_BY).equals(APIManager.JWT)){
                btnEditPassword.setVisibility(View.GONE);
            }
        }else{
            btnFollow.setVisibility(View.VISIBLE);
            llButton.setVisibility(View.GONE);
        }

        MainActivity.getTitleToolBar().setVisibility(View.VISIBLE);
        MainActivity.getTitleToolBar().setText(R.string.app_name);
//        MainActivity.getToolBar().inflateMenu(R.menu.menu_default);

        updateUI();

    }

    private void updateUI() {
        if(user == null) return;
        Uri photoUri;
        if(user.profile_picture != null){
             photoUri = Uri.parse(user.getTempUrlPic());
        }else{
            photoUri = new Uri.Builder()
                    .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                    .path(String.valueOf(R.drawable.no_image_available))
                    .build();
        }
        ivProfilePicture.setImageURI(photoUri);
        tvDisplayName.setText(user.name);
        tvBio.setText(user.bio);
        tvAccount.setText("@" + user.username);
        tvFollower.setText("Followers: " + user.total_followers);
        tvFollowing.setText("Following: " + user.total_following);
        if(!isOwn)
            updateBtnFollow();
    }

    public void updateUI(User user) {
        this.user = user;
        updateUI();
    }

    private void follow(){
        isFollow = true;
        updateBtnFollow();
        if(followCall != null && followCall.isExecuted()){
            followCall.cancel();
        }
        followCall = APIManager.getUserRepo().follow(APIManager.getTokenReqeust(),user.id);
        followCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Log.d(TAG, "onResponse: ");
                    User tmpUser = response.body();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    private void updateBtnFollow() {
        if(isFollow){
            btnFollow.setText("Following");
        }else{
            btnFollow.setText("Follow");
        }
    }

    private void unFollow(){
        isFollow = false;
        updateBtnFollow();
        if(followCall != null && followCall.isExecuted()){
            followCall.cancel();
        }
        followCall = APIManager.getUserRepo().unFollow(APIManager.getTokenReqeust(),user.id);
        followCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Log.d(TAG, "onResponse: ");
                    User tmpUser = response.body();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public static UserFragment newInstance(Boolean isOwn) {
        Bundle args = new Bundle();
        args.putBoolean("isOwn",isOwn);
        UserFragment fragment = new UserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static UserFragment newInstance(Boolean isOwn,User user) {
        Bundle args = new Bundle();
        args.putBoolean("isOwn",isOwn);
        args.putString("user",GlobalClass.JSONtoString(user));
        UserFragment fragment = new UserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            if(isOwn){
                user = GlobalClass.getObjectFromJson(SessionManager.getInstance().getValueString(SessionManager.USER),User.class);
                updateUI();
            }
        }else{

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_logout:
                SessionManager.getInstance().logout();
                getActivity().finish();
                Intent i = new Intent(getContext(), LoginActivity.class);
                getActivity().startActivity(i);
                break;
            case R.id.btn_edit_password:
                mFragmentNavigation.pushFragment(EditPasswordFragment.newInstance());
                break;
            case R.id.btn_edit_profile:
                mFragmentNavigation.pushFragment(EditProfileFragment.newInstance(user));
                break;
            case R.id.btn_follow:
                if(isFollow)
                    unFollow();
                else
                    follow();
                break;
            case R.id.tv_follower:
                mFragmentNavigation.pushFragment(FollowersFragment.newInstance(FollowersFragment.FOLLOWERS,user.id));
                break;
            case R.id.tv_following:
                mFragmentNavigation.pushFragment(FollowersFragment.newInstance(FollowersFragment.FOLLOWING,user.id));
                break;
        }
    }

    public interface UpdateProfileInterface{
        public void onUpdateProfile(User user);
    }
}
