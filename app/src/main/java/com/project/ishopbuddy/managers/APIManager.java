package com.project.ishopbuddy.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.services.ProductRepo;
import com.project.ishopbuddy.services.ReviewRepo;
import com.project.ishopbuddy.services.SearchRepo;
import com.project.ishopbuddy.services.UserRepo;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mchen on 10/10/2016.
 */

public class APIManager {
//    public static final String BASE_URL = "http://www.shoupbud.xyz/";
    private static final String BASE_URL = "http://manumeda.id/";
    public static final String GRANT_TYPE = "password";
    public static final int CLIENT_ID = 2;
    public static final String SECRET_KEY = "LsqkkqsnJSNiBfu302nRkoZyYpUrCclnP5hmPUa0";
    public static final String JWT = "token";
    private static Retrofit retrofit = null;

    private static UserRepo userRepo;
    private static ProductRepo productRepo;
    private static ReviewRepo reviewRepo;
    private static SearchRepo searchRepo;

    public static Retrofit getClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static String getTokenReqeust(){
        return "Bearer " + SessionManager.getInstance().getValueString(SessionManager.KEY_TOKEN);
    }

    public static UserRepo getUserRepo(){
        if(userRepo == null){
            userRepo = getClient().create(UserRepo.class);
        }
        return userRepo;
    }

    public static ProductRepo getProductRepo(){
        if(productRepo == null){
            productRepo = getClient().create(ProductRepo.class);
        }
        return productRepo;
    }

    public static ReviewRepo getReviewRepo(){
        if(reviewRepo == null){
            reviewRepo = getClient().create(ReviewRepo.class);
        }
        return reviewRepo;
    }

    public static SearchRepo getSearchRepo(){
        if(searchRepo == null){
            searchRepo = getClient().create(SearchRepo.class);
        }
        return searchRepo;
    }

}
