package com.project.ishopbuddy.managers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.GlobalClass;
import com.project.ishopbuddy.model.ReqToken;
import com.project.ishopbuddy.model.Token;
import com.project.ishopbuddy.model.User;
import com.project.ishopbuddy.view.activities.LoginActivity;
import com.project.ishopbuddy.view.activities.MainActivity;
import com.project.ishopbuddy.view.activities.SplashActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mchen on 10/16/2016.
 */

public class SessionManager {
    private static final String PREF_NAME = "iShoupBud";
    public static final String KEY_TOKEN = "token";
    public static final String LOGIN_BY = "login by";
    public static final String USER = "user";

    public static String TAG = "tmp";

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    private WeakReference<Context> contextRef;
    private String loginType;

    private static SessionManager mInstance;

    public SessionManager(){

    }

    public void initialSession(Context context){
        sharedpreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }

    public void save(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }

    public void save(String key,Boolean value){
        editor.putBoolean(key,value);
        editor.commit();
    }

    public void save(String key,int value){
        editor.putInt(key,value);
        editor.commit();
    }

    public void save(String key,float value){
        editor.putFloat(key,value);
        editor.commit();
    }

    public String getValueString(String key){
        return sharedpreferences.getString(key,"");
    }

    public int getValueint(String key){
        return sharedpreferences.getInt(key,0);
    }

    public boolean getValueBool(String key){
        return sharedpreferences.getBoolean(key,false);
    }

    public void logout(){
        if(getValueString(LOGIN_BY).equals(FacebookAPIManager.FACEBOOK)){
            FacebookAPIManager.getInstance().logout();
        }else if(getValueString(LOGIN_BY).equals(GoogleApiManager.GOOGLE)){
            GoogleApiManager.getGoogleApi().disconnect();
        }
        save(KEY_TOKEN,"");
        save(LOGIN_BY,"");
    }

    public void getUser(){
        Call<User> getUser = APIManager.getUserRepo().getProfile(APIManager.getTokenReqeust());
        getUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    User user = response.body();
                    SessionManager.getInstance().save(SessionManager.USER, GlobalClass.JSONtoString(user));
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        Token token = gson.fromJson(response.errorBody().string(),Token.class);
                        Log.d(TAG, "onResponse: " + token.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(contextRef.get(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public void login(Context context, final String loginType, String email, String password){
        Log.d(TAG, "login: ");
        ReqToken reqToken = new ReqToken(APIManager.GRANT_TYPE, APIManager.CLIENT_ID, APIManager.SECRET_KEY, email, password);
        Call<Token> loginForToken = APIManager.getUserRepo().login(reqToken);
        this.contextRef = new WeakReference<>(context);
        this.loginType = loginType;
        loginForToken.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Token token = response.body();
                    SessionManager.getInstance().save(SessionManager.KEY_TOKEN,token.access_token);
                    SessionManager.getInstance().save(SessionManager.LOGIN_BY,SessionManager.getInstance().loginType);
                    Intent i = new Intent(contextRef.get(),MainActivity.class);
                    Log.d(TAG, "onResponse: " + contextRef.get().getClass().toString());
                    ((LoginActivity)contextRef.get()).finish();
                    contextRef.get().startActivity(i);
                    getUser();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        Token token = gson.fromJson(response.errorBody().string(),Token.class);
                        Log.d(TAG, "onResponse: " + token.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(contextRef.get(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public void login(final Context context, String loginType, String token){
        Log.d(TAG, "login: ");
        HashMap<String,String> map = new HashMap<>();
        map.put("type",loginType);
        map.put("token",token);
        final Call<Token> loginForToken = APIManager.getUserRepo().socialAuth(map);
        this.contextRef = new WeakReference<>(context);
        this.loginType = loginType;
        Log.d(TAG, "onResponse: class " + this.contextRef.get().getClass().getSimpleName());
        loginForToken.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Token token = response.body();
                    SessionManager.getInstance().save(SessionManager.KEY_TOKEN,token.access_token);
                    SessionManager.getInstance().save(SessionManager.LOGIN_BY,SessionManager.getInstance().loginType);
                    Intent i = new Intent(contextRef.get(),MainActivity.class);
                    if(contextRef.get().getClass().getSimpleName().equals("SplashActivity"))
                        ((SplashActivity)contextRef.get()).finish();
                    else
                        ((LoginActivity)contextRef.get()).finish();
                    contextRef.get().startActivity(i);
                    getUser();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        Token token = gson.fromJson(response.errorBody().string(),Token.class);
                        Log.d(TAG, "onResponse: " + token.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    if(SessionManager.getInstance().loginType.equals(FacebookAPIManager.FACEBOOK)){
                        FacebookAPIManager.getInstance().logout();
                    }else if(SessionManager.getInstance().loginType.equals(GoogleApiManager.GOOGLE)){
                        GoogleApiManager.getGoogleApi().disconnect();
                    }
                    Toast.makeText(contextRef.get(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public static SessionManager getInstance(){
        if(mInstance == null){
          mInstance = new SessionManager();
        }
        return mInstance;
    }
}
