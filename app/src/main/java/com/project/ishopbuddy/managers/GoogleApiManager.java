package com.project.ishopbuddy.managers;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.view.activities.MainActivity;

import java.lang.ref.WeakReference;

/**
 * Created by MICHAEL on 9/9/2016.
 */
public class GoogleApiManager implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{
    private static final String TAG = "tmp";
    public static final String GOOGLE = "g+";

    private static GoogleApiManager mInstance;

    @Nullable
    private GoogleApiClient googleApiClient;

    private WeakReference<AppCompatActivity> activityRef;

    public static final int REQUEST_CODE_RESOLUTION = 1;
    public static final int RC_SIGN_IN = 2;

    private GoogleSignInAccount googleSignInAccount;

    private GoogleApiManager(){
        mInstance = this;
    }

    public void initGoogleAPI(AppCompatActivity activity){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("562232115481-rvb8cht8184no0m9m44t9mb9ql262cfk.apps.googleusercontent.com")
                .build();
        if(googleApiClient == null){
            googleApiClient = new GoogleApiClient.Builder(activity)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        this.activityRef = new WeakReference<>(activity);
        Log.d(TAG, "initGoogleAPI: ");
    }

    public void connect(){
        if(googleApiClient == null){
            throw new IllegalStateException("google client need to be initialize");
        }else{
            Log.d(TAG, "connect: ");
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            activityRef.get().startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    public void disconnect(){
        if(googleApiClient == null){
            throw new IllegalStateException("google client need to be initialize");
        }else{
            Log.d(TAG, "disconnect: ");
            googleApiClient.disconnect();
        }
    }

    public void reconnect(){
        if(googleApiClient == null){
            throw new IllegalStateException("google client need to be initialize");
        }else{
            Log.d(TAG, "reconnect: ");
            googleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
        }
    }

    public GoogleApiClient getGoogleApiClient(){
        if(googleApiClient == null){
            throw new IllegalStateException("google client need to be initialize");
        }
        else{
            return googleApiClient;
        }
    }

    public void setResultAccount(GoogleSignInResult googleSignInResult){
        this.googleSignInAccount = googleSignInResult.getSignInAccount();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: ");
        if (!connectionResult.hasResolution()) {
            // show the localized error dialog.
            GoogleApiAvailability.getInstance().getErrorDialog(activityRef.get(), connectionResult.getErrorCode(), 0).show();
            return;
        }
        try {
            connectionResult.startResolutionForResult(activityRef.get(), REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected: " + googleSignInAccount.getIdToken());
        SessionManager.getInstance().login(activityRef.get(),GOOGLE,googleSignInAccount.getIdToken());
        /*SessionManager.getInstance().save(SessionManager.LOGIN_BY,GOOGLE);
        activity.finish();
        Intent i = new Intent(activity.getApplicationContext(),MainActivity.class);
        activity.startActivity(i);*/
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: ");
    }

    @NonNull
    public static synchronized GoogleApiManager getGoogleApi() {
        if(mInstance == null){
            mInstance = new GoogleApiManager();
        }
        return mInstance;
    }
}
