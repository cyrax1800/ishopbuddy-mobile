package com.project.ishopbuddy.managers;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.google.android.gms.vision.Frame;
import com.project.ishopbuddy.view.activities.MainActivity;
import com.roughike.bottombar.BottomBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by MICHAEL on 8/20/2016.
 */
public class FragmentNavigator {

    //Declare the constants
    public static final int TAB1 = 0;
    public static final int TAB2 = 1;
    public static final int TAB3 = 2;
    public static final int TAB4 = 3;
    public static final int TAB5 = 4;

    private FragmentManager mFragmentManager;
    int mContainerId;
    private final List<Stack<Fragment>> mFragmentStacks;
    int mSelectedTabIndex = -1;
    private List<Integer> tabPQ = new ArrayList<>();
    public BottomBar mBottomBar;
    public AppBarLayout mAppBarLayout;

    public FragmentNavigator(@NonNull FragmentManager fragmentManager, @IdRes int LayoutId, List<Fragment> fragments, BottomBar bottomBar, AppBarLayout appBarLayout){
        mFragmentManager = fragmentManager;
        mContainerId = LayoutId;
        mFragmentStacks = new ArrayList<>(fragments.size());
        mBottomBar = bottomBar;
        mAppBarLayout = appBarLayout;

        for (Fragment fragment : fragments) {
            Stack<Fragment> stack = new Stack<>();
            stack.add(fragment);
            mFragmentStacks.add(stack);
        }

        mSelectedTabIndex = -1;
//        FragmentTransaction ft = mFragmentManager.beginTransaction();
//        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        ft.add(mContainerId,mFragmentStacks.get(mSelectedTabIndex).peek(),mSelectedTabIndex + "_" + generateTag(mFragmentStacks.get(mSelectedTabIndex).peek()));
//        ft.commit();
    }

    public void switchTab(int index,boolean isBack){
        if (mSelectedTabIndex != index) {
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if(mSelectedTabIndex > -1){
                if(mFragmentStacks.get(mSelectedTabIndex).peek().isAdded())
                    ft.hide(mFragmentStacks.get(mSelectedTabIndex).peek());
                if(mFragmentStacks.get(index).peek().isAdded())
                    ft.show(mFragmentStacks.get(index).peek());
                else{
                    ft.add(mContainerId,mFragmentStacks.get(index).peek(),index + "_" + generateTag(mFragmentStacks.get(index).peek()));
                }
            }else{
                ft.add(mContainerId,mFragmentStacks.get(index).peek(),index + "_" + generateTag(mFragmentStacks.get(index).peek()));
            }
            ft.commit();
            mSelectedTabIndex = index;
            mBottomBar.selectTabAtPosition(mSelectedTabIndex,false);
            mAppBarLayout.setExpanded(true, false);
            MainActivity.getToolBar().getMenu().clear();
        }
    }

    public void switchTabWithoutPushTab(int index){
        if (mSelectedTabIndex != index) {
            /*if(tabPQ.indexOf(mSelectedTabIndex) > -1){
                int tmpTab = tabPQ.remove(tabPQ.indexOf(mSelectedTabIndex));
                tabPQ.add(tmpTab);
            }else{
                tabPQ.add(mSelectedTabIndex);
            }*/
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if(mSelectedTabIndex > -1){
                if(mFragmentStacks.get(mSelectedTabIndex).peek().isAdded())
                    ft.hide(mFragmentStacks.get(mSelectedTabIndex).peek());
                if(mFragmentStacks.get(index).peek().isAdded())
                    ft.show(mFragmentStacks.get(index).peek());
                else{
                    ft.add(mContainerId,mFragmentStacks.get(index).peek(),index + "_" + generateTag(mFragmentStacks.get(index).peek()));
                }
            }else{
                ft.add(mContainerId,mFragmentStacks.get(index).peek(),index + "_" + generateTag(mFragmentStacks.get(index).peek()));
            }
            ft.commitAllowingStateLoss();
            mSelectedTabIndex = index;
            mAppBarLayout.setExpanded(true, false);
            MainActivity.getToolBar().getMenu().clear();
        }
    }

    public void switchTab(int index){
        if (mSelectedTabIndex != index) {
            if(tabPQ.indexOf(mSelectedTabIndex) > -1){
                int tmpTab = tabPQ.remove(tabPQ.indexOf(mSelectedTabIndex));
                tabPQ.add(tmpTab);
            }else{
                tabPQ.add(mSelectedTabIndex);
            }
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if(mSelectedTabIndex > -1){
                if(mFragmentStacks.get(mSelectedTabIndex).peek().isAdded())
                    ft.hide(mFragmentStacks.get(mSelectedTabIndex).peek());
                if(mFragmentStacks.get(index).peek().isAdded())
                    ft.show(mFragmentStacks.get(index).peek());
                else{
                    ft.add(mContainerId,mFragmentStacks.get(index).peek(),index + "_" + generateTag(mFragmentStacks.get(index).peek()));
                }
            }else{
                ft.add(mContainerId,mFragmentStacks.get(index).peek(),index + "_" + generateTag(mFragmentStacks.get(index).peek()));
            }
            ft.commitAllowingStateLoss();
            mSelectedTabIndex = index;
            mAppBarLayout.setExpanded(true, false);
            MainActivity.getToolBar().getMenu().clear();
        }
    }

    public void push(Fragment fragment){
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.hide(mFragmentStacks.get(mSelectedTabIndex).peek());
        ft.add(mContainerId,fragment,mSelectedTabIndex + "_" + generateTag(fragment));
        ft.commit();
        mFragmentStacks.get(mSelectedTabIndex).push(fragment);
    }

    public Fragment getCurrentFragment(){
        return mFragmentStacks.get(mSelectedTabIndex).peek();
    }

    private String generateTag(Fragment fragment) {
        return fragment.getClass().getName();
    }

    public void pop(){

    }

    public boolean onBackPress(){
        mAppBarLayout.setExpanded(true, false);

        Stack<Fragment> fragmentStack = mFragmentStacks.get(mSelectedTabIndex);

        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        if(fragmentStack.size() > 1){
            Fragment currentFrag = fragmentStack.pop();
            ft.hide(currentFrag);
            ft.show(mFragmentStacks.get(mSelectedTabIndex).peek());
            ft.commit();
            return true;
        }else{
            if(tabPQ.size() > 1){
                int tmpTab = tabPQ.remove(tabPQ.size() - 1);
                if(tabPQ.indexOf(mSelectedTabIndex) > -1){
                    tabPQ.remove(tabPQ.indexOf(mSelectedTabIndex));
                }
                switchTab(tmpTab,true);
                return true;
            }else{
                return false;
            }
        }
    }

    public void clearStack(int tab){
        Stack<Fragment> fragmentStack = mFragmentStacks.get(tab);

        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        while(fragmentStack.size() > 1){
            Fragment currentFrag = fragmentStack.pop();
            ft.remove(currentFrag);
        }
        ft.show(mFragmentStacks.get(tab).peek());
        ft.commit();
//        if(tabPQ.size() > 1){
//            int tmpTab = tabPQ.remove(tabPQ.size() - 1);
//        }
    }

    public interface FragmentNavigation {
        public void pushFragment(Fragment fragment);
    }
}
