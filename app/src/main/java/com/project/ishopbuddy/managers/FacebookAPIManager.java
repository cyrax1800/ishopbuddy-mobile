package com.project.ishopbuddy.managers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.project.ishopbuddy.view.activities.MainActivity;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by MICHAEL on 10/5/2016.
 */
public class FacebookAPIManager {
    private static String TAG = "tmp";
    public final static int FACEBOOK_REQUESTCODE = 3;
    public final static String FACEBOOK = "fb";

    private static FacebookAPIManager mInstance;
    public CallbackManager callbackManager;
    public FacebookCallback<LoginResult> loginCallback;
    public ResultCallback resultCallback;
    public WeakReference<AppCompatActivity> activityRef;

    private FacebookAPIManager(){

    }

    public void initialFacebookSDK(final Context context){
        FacebookSdk.sdkInitialize(context,FACEBOOK_REQUESTCODE);
        callbackManager = CallbackManager.Factory.create();
        loginCallback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "onSuccess: Facebook Login Success ");
                FacebookAPIManager.getInstance().getUserProfile();
                SessionManager.getInstance().login(activityRef.get(),FACEBOOK,loginResult.getAccessToken().getToken());
                /*if(resultCallback != null){
                    resultCallback.result();
                }*/
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "onCancel: Facebook Cancel Login");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "onError: Facebook Login Error : " + error.toString());
            }
        };
    }

    public void setContext(AppCompatActivity activity){
        activityRef = new WeakReference<>(activity);
    }

    public void getUserProfile(){
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        Log.d(TAG, "onCompleted: " + object);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public boolean isConnected(){
        return AccessToken.getCurrentAccessToken() != null;
    }

    public void logout(){
        LoginManager.getInstance().logOut();
    }

    public static synchronized FacebookAPIManager getInstance() {
        if(mInstance == null){
            mInstance = new FacebookAPIManager();
        }
        return mInstance;
    }

    public interface ResultCallback{
        public void result();
    }
}
