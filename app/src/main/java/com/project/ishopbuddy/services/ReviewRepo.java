package com.project.ishopbuddy.services;

import com.project.ishopbuddy.model.Review;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by michael on 06/12/16.
 */

public interface ReviewRepo {

    @Headers("Accept: application/json")
    @GET("api/product/{idproduct}/review")
    Call<List<Review>>  getReviewProductFrom(@Header("Authorization") String authorization, @Path("idproduct") int productId);

    @Headers("Accept: application/json")
    @POST("api/review")
    Call<Review> sendReview(@Header("Authorization") String authorization,@Body Review review);

    @Headers("Accept: application/json")
    @PUT("api/review/{id_review}")
    Call<Review> editReview(@Header("Authorization") String authorization,@Path("id_review") int idReview, @Body Review review);

    @Headers("Accept: application/json")
    @DELETE("api/review/{id_review}")
    Call<Review> deleteReview(@Header("Authorization") String authorization,@Path("id_review") int idReview);
}
