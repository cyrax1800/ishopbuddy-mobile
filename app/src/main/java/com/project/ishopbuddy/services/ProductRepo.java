package com.project.ishopbuddy.services;

import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Product;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by mchen on 11/18/2016.
 */

public interface ProductRepo {
    @Multipart
    @Headers("Accept: application/json")
    @POST("api/product")
    Call<Product> addProduct(@Header("Authorization") String authorization,
                             @Part("itemname") RequestBody name,
                             @Part("barcode") RequestBody barcode,
                             @Part("rating") float rating,
                             @Part("hasimage") boolean hasImage,
                             @Part("review") RequestBody review,
                             @Part("category_id") int categoryId,
                             @Part MultipartBody.Part picture);

    @Headers("Accept: application/json")
    @GET("api/product")
    Call<List<Product>> getAllProduct(@Header("Authorization") String authorization);

    @Headers("Accept: application/json")
    @GET("/api/product")
    Call<List<Product>> getProductByPage(@Header("Authorization") String authorization, @Query("page") int page);

    @Headers("Accept: application/json")
    @GET("/api/category/{category_id}/recomendation")
    Call<List<Product>> getProductByCategory(@Header("Authorization") String authorization, @Path("category_id") int categoryId);

    @Headers("Accept: application/json")
    @GET("api/product/barcode/{barcode}")
    Call<Product> getProductByBarcode(@Header("Authorization") String authorization,@Path("barcode") String barcode);
}
