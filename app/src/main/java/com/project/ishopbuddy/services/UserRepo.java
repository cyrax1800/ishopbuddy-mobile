package com.project.ishopbuddy.services;

import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.ReqToken;
import com.project.ishopbuddy.model.ResponsRegis;
import com.project.ishopbuddy.model.Token;
import com.project.ishopbuddy.model.User;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by mchen on 10/10/2016.
 */

public interface UserRepo {

    @Headers("Accept: application/json")
    @POST("api/login")
    Call<Token> login(@Body ReqToken reqToken);

    @Headers("Accept: application/json")
    @POST("api/socialauth")
    Call<Token> socialAuth(@Body HashMap request);

    @Headers("Accept: application/json")
    @POST("api/register")
    Call<ResponsRegis> register(@Body HashMap map);

    @Headers("Accept: application/json")
    @PUT("api/me/changepwd")
    Call<BaseRespond> changePassword(@Header("Authorization") String authorization, @Body HashMap map);

    @Multipart
    @Headers("Accept: application/json")
    @POST("api/me")
    Call<User> updateProfile(@Header("Authorization") String authorization,
                                    @Part("username") RequestBody username,
                                    @Part("name") RequestBody name,
                                    @Part("bio") RequestBody bio,
                                    @Part MultipartBody.Part picture);

    @Headers("Accept: application/json")
    @GET("api/me")
    Call<User> getProfile(@Header("Authorization") String authorization);

    @Headers("Accept: application/json")
    @GET("api/me/followers")
    Call<List<User>> getFollowers(@Header("Authorization") String authorization);

    @Headers("Accept: application/json")
    @GET("api/user/{user_id}/following")
    Call<List<User>> getFollowing(@Header("Authorization") String authorization, @Path("user_id") int userID);

    @Headers("Accept: application/json")
    @POST("api/user/{user_id}/follow")
    Call<User> follow(@Header("Authorization") String authorization, @Path("user_id") int userID);

    @Headers("Accept: application/json")
    @DELETE("api/user/{user_id}/follow")
    Call<User> unFollow(@Header("Authorization") String authorization, @Path("user_id") int userID);
}
