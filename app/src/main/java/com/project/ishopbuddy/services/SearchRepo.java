package com.project.ishopbuddy.services;

import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Search;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by michael on 12/12/16.
 */

public interface SearchRepo {

    @Headers("Accept: application/json")
    @GET("/api/search")
    Call<List<Object>> search(@Header("Authorization") String authorization, @Query("q") String keyword);
}
