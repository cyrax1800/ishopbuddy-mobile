package com.project.ishopbuddy.view.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.FacebookAPIManager;
import com.project.ishopbuddy.managers.GoogleApiManager;
import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.ReqToken;
import com.project.ishopbuddy.model.Token;
import com.project.ishopbuddy.managers.APIManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    public static final int REQUESTCODE_REGISTER = 4;
    public static final int RESULTCODE_REGISTER = 5;

    EditText etEmail,etPassword;
    TextView tvSignup;
    AppCompatButton loginBtn;
    LoginButton fb_loginBtn;
    SignInButton google_loginBtn;

    String email,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookAPIManager.getInstance().setContext(LoginActivity.this);
        setContentView(R.layout.activity_login);

        GoogleApiManager.getGoogleApi().initGoogleAPI(LoginActivity.this);
        initView();
        fb_loginBtn.setReadPermissions("email");
        fb_loginBtn.registerCallback(FacebookAPIManager.getInstance().callbackManager,FacebookAPIManager.getInstance().loginCallback);
        /*FacebookAPIManager.getInstance().resultCallback = new FacebookAPIManager.ResultCallback() {
            @Override
            public void result() {
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                finish();
                startActivity(i);
            }
        };*/

        google_loginBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        tvSignup.setOnClickListener(this);
    }

    private void initView(){
        fb_loginBtn = (LoginButton)findViewById(R.id.facebook_login_button);
        google_loginBtn = (SignInButton)findViewById(R.id.google_login_button);
        loginBtn = (AppCompatButton) findViewById(R.id.login_button);
        tvSignup = (TextView) findViewById(R.id.register_tv);
        etEmail = (EditText) findViewById(R.id.username_et);
        etPassword = (EditText) findViewById(R.id.password_et);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: Masuk yang Activity");
        if(requestCode == FacebookAPIManager.FACEBOOK_REQUESTCODE){
            FacebookAPIManager.getInstance().callbackManager.onActivityResult(requestCode,resultCode,data);
        }else if (requestCode == GoogleApiManager.REQUEST_CODE_RESOLUTION) {
            Log.d(TAG, "onActivityResult: REQUEST_CODE_RESOLUTION");
            if (!GoogleApiManager.getGoogleApi().getGoogleApiClient().isConnecting()) {
                Log.d(TAG, "onActivityResult:  if (!AppController.getGoogleApi().getGoogleApiClient().isConnecting())");
                GoogleApiManager.getGoogleApi().connect();
            }
        }else if (requestCode == GoogleApiManager.RC_SIGN_IN) {
            Log.d(TAG, "onActivityResult: RC_SIGN_IN");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }else if (requestCode == REQUESTCODE_REGISTER) {
            if(resultCode == RESULTCODE_REGISTER){
                email = data.getStringExtra("email");
                password = data.getStringExtra("password");
                SessionManager.getInstance().login(this,APIManager.JWT,email,password);
            }
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            if(!GoogleApiManager.getGoogleApi().getGoogleApiClient().isConnected()){
                GoogleApiManager.getGoogleApi().setResultAccount(result);
                Log.d(TAG, "handleSignInResult:  AppController.getGoogleApi().reconnect()");
                GoogleApiManager.getGoogleApi().reconnect();
            }
        } else {
            // Signed out, show unauthenticated UI.
            Toast.makeText(getApplicationContext(),"Login Google Fail",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_button:
                email = etEmail.getEditableText().toString();
                password = etPassword.getEditableText().toString();
                SessionManager.getInstance().login(this,APIManager.JWT,email,password);
                break;
            case R.id.google_login_button:
                GoogleApiManager.getGoogleApi().connect();
                break;
            case R.id.register_tv:
                Intent i = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivityForResult(i,REQUESTCODE_REGISTER);
                break;
            default:
                break;
        }
    }
}
