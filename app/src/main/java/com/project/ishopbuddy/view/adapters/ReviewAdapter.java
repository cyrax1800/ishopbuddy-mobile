package com.project.ishopbuddy.view.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.common.util.UriUtil;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.model.Review;
import com.project.ishopbuddy.view.holders.ReviewHolder;

import java.util.List;

/**
 * Created by mchen on 11/27/2016.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewHolder> {

    private List<Review> reviews;

    private Uri photoUri;

    public ReviewAdapter(List<Review> reviews){
        photoUri = new Uri.Builder()
                .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                .path(String.valueOf(R.drawable.no_image_available))
                .build();
        this.reviews = reviews;
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_review,parent,false);
        return new ReviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReviewHolder holder, int position) {
        Review review = reviews.get(position);
        if(review.user.profile_picture != null){
            Uri uri = Uri.parse(review.user.getTempUrlPic());
            holder.sdvProfilePicture.setImageURI(uri);
        }else{
            holder.sdvProfilePicture.setImageURI(photoUri);
        }
        holder.tvUsername.setText(review.user.name);
        holder.tvReview.setText(review.review);
        holder.ratingBar.setRating(review.rating);
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }
}
