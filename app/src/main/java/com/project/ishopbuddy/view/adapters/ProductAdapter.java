package com.project.ishopbuddy.view.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.common.util.UriUtil;
import com.facebook.imagepipeline.request.ImageRequest;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.listeners.OnLoadMoreListener;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.model.Review;
import com.project.ishopbuddy.view.holders.LoadingViewHolder;
import com.project.ishopbuddy.view.holders.ProductHolder;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by mchen on 11/22/2016.
 */

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static String TAG = "tmp";

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<Product> _products;

    private Uri photoUri;

    public boolean isLoading;
    public int visibleThreshold = 5;
    public int page = 1;

    public int lastVisibleItem, totalItemCount, lastTotalItem;

    private OnLoadMoreListener mOnLoadMoreListener;

    public ProductAdapter(List<Product> products){
        photoUri = new Uri.Builder()
                .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                .path(String.valueOf(R.drawable.no_image_available))
                .build();
        this._products = products;
    }

    @Override
    public int getItemViewType(int position) {
//        Log.d(TAG, "getItemViewType: " + (_products.get(position) == null));
        return _products.get(position).name == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_product,parent,false);
            return new ProductHolder(itemView);
        }else if(viewType == VIEW_TYPE_LOADING){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_loading,parent,false);
            return new LoadingViewHolder(itemView);
        }
        return  null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        Log.d(TAG, "onBindViewHolder: " + position);
        if (holder instanceof ProductHolder){
            ProductHolder productHolder = (ProductHolder)holder;
            Product product = _products.get(position);
            if(product.picture != null){
                Uri imageUri = Uri.parse(product.getTempUrlPic());
                productHolder.ivProduct.setImageURI(imageUri);
            }else{
                productHolder.ivProduct.setImageURI(photoUri);
            }
            productHolder.txtItemName.setText(product.name);
            if(product.user != null){
                productHolder.txtUserName.setText("@" + product.user.name);
            }
            productHolder.txtReviewValue.setText(String.valueOf(product.rating));
            productHolder.txtReviewTotal.setText(String.valueOf(product.total_review));
            productHolder.ratingBar.setRating(product.rating);
            if(product.review != null){
                Review review = product.review.get(0);
                if(review.user.profile_picture != null && !review.user.profile_picture.equals("")){
                    Uri imageUri = Uri.parse(review.user.getTempUrlPic());
                    productHolder.ivProfilePicture.setImageURI(imageUri);
                }else{
                    productHolder.ivProfilePicture.setImageURI(photoUri);
                }
                productHolder.txtReviewUsername.setText(review.user.name);
                productHolder.txtReviewUser.setText(review.review);
                productHolder.ratingBarReview.setRating(review.rating);
            }
        }else if(holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public int getItemCount() {
        return _products == null ? 0 :_products.size();
    }

    public OnLoadMoreListener getmOnLoadMoreListener() {
        return mOnLoadMoreListener;
    }
}
