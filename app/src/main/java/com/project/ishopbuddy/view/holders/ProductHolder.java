package com.project.ishopbuddy.view.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.ishopbuddy.R;

/**
 * Created by mchen on 11/22/2016.
 */

public class ProductHolder extends RecyclerView.ViewHolder {

    public SimpleDraweeView ivProduct,ivProfilePicture;
    public TextView txtItemName,txtUserName,txtReviewValue,txtReviewTotal,txtReviewUser,txtReviewUsername;
    public RatingBar ratingBar,ratingBarReview;

    public ProductHolder(View itemView) {
        super(itemView);
        ivProduct = (SimpleDraweeView) itemView.findViewById(R.id.iv_image_product);
        ivProfilePicture = (SimpleDraweeView) itemView.findViewById(R.id.iv_profile_picture);
        txtItemName = (TextView) itemView.findViewById(R.id.hol_product_name);
        txtUserName = (TextView) itemView.findViewById(R.id.hol_product_user);
        txtReviewValue = (TextView) itemView.findViewById(R.id.hol_product_rating_text);
        txtReviewTotal = (TextView) itemView.findViewById(R.id.hol_product_user_count);
        txtReviewUsername = (TextView) itemView.findViewById(R.id.tv_review_username);
        txtReviewUser = (TextView) itemView.findViewById(R.id.tv_review_user_review);
        ratingBar = (RatingBar)itemView.findViewById(R.id.hol_product_rating_bar);
        ratingBarReview = (RatingBar)itemView.findViewById(R.id.ratbar_review);
    }
}
