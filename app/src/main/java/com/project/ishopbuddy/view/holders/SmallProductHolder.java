package com.project.ishopbuddy.view.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.ishopbuddy.R;

/**
 * Created by michael on 21/12/16.
 */

public class SmallProductHolder extends RecyclerView.ViewHolder {

    public SimpleDraweeView ivProduct;
    public TextView txtItemName;
    public RatingBar ratingBar;

    public SmallProductHolder(View itemView) {
        super(itemView);
        ivProduct = (SimpleDraweeView)itemView.findViewById(R.id.iv_image_product);
        txtItemName = (TextView) itemView.findViewById(R.id.tv_product_name);
        ratingBar = (RatingBar) itemView.findViewById(R.id.hol_product_rating_bar);
    }
}
