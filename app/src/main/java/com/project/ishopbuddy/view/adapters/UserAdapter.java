package com.project.ishopbuddy.view.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.common.util.UriUtil;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.listeners.ChangeBlockListener;
import com.project.ishopbuddy.model.User;
import com.project.ishopbuddy.view.holders.UserHolder;

import java.util.List;

/**
 * Created by michael on 12/12/16.
 */

public class UserAdapter extends RecyclerView.Adapter<UserHolder> {

    public List<User> users;
    private boolean isFollowAdapter;

    ChangeBlockListener changeBlockListener;

    private Uri photoUri;

    public UserAdapter(List<User> users, Boolean isFollowAdapter){
        photoUri = new Uri.Builder()
                .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                .path(String.valueOf(R.drawable.no_image_available))
                .build();
        this.isFollowAdapter = isFollowAdapter;
        this.users = users;
    }

    public void setChangeBlockListener(ChangeBlockListener changeBlockListener){
        this.changeBlockListener = changeBlockListener;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_user,parent,false);
        return new UserHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, final int position) {
        User user = users.get(position);
        if(user.profile_picture != null){
            Uri uri = Uri.parse(user.getTempUrlPic());
            holder.ivProfilePicture.setImageURI(uri);
        }else{
            holder.ivProfilePicture.setImageURI(photoUri);
        }
        holder.tvAccountName.setText(user.name);
        if(user.bio != null){
            holder.tvBio.setVisibility(View.GONE);
        }else{
            holder.tvBio.setVisibility(View.VISIBLE);
            holder.tvBio.setText(user.bio);
        }
        if(!isFollowAdapter)
            holder.btnFollow.setVisibility(View.GONE);
        else{
            if(user.is_following){
                holder.btnFollow.setText("Following");
            }else{
                holder.btnFollow.setText("Follow");
            }
        }

        holder.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBlocked(position);
            }
        });
    }

    public void changeBlocked(int position) {
        User user = users.get(position);
        if(user.is_following){
            user.is_following = false;
            changeBlockListener.changeBlock(false,user.id);
        }
        else{
            user.is_following = true;
            changeBlockListener.changeBlock(true,user.id);
        }
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
