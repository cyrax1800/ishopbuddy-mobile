package com.project.ishopbuddy.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.project.ishopbuddy.R;
import com.project.ishopbuddy.model.Category;

import java.util.List;

/**
 * Created by michael on 21/12/16.
 */

public class CategoryAdapter extends ArrayAdapter<Category> {

    private List<Category> categories;

    public CategoryAdapter(Context context, List<Category> objects) {
        super(context,0, objects);
        this.categories = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_list_view_category, parent, false);
        }
        // Lookup view for data population
        TextView text = (TextView) convertView.findViewById(R.id.text);
        text.setText(categories.get(position).getName());

        return convertView;
    }
}
