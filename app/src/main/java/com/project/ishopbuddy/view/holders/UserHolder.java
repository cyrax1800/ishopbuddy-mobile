package com.project.ishopbuddy.view.holders;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.ishopbuddy.R;

/**
 * Created by michael on 12/12/16.
 */

public class UserHolder extends RecyclerView.ViewHolder {

    public SimpleDraweeView ivProfilePicture;
    public TextView tvAccountName, tvBio;
    public AppCompatButton btnFollow;

    public UserHolder(View itemView) {
        super(itemView);
        ivProfilePicture = (SimpleDraweeView) itemView.findViewById(R.id.iv_profile_picture);
        tvAccountName = (TextView) itemView.findViewById(R.id.tv_username);
        tvBio = (TextView) itemView.findViewById(R.id.tv_user_bio);
        btnFollow = (AppCompatButton) itemView.findViewById(R.id.btn_follow);
    }
}
