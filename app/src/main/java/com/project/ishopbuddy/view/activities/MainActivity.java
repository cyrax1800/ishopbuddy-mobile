package com.project.ishopbuddy.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.fragments.AddProductFragment;
import com.project.ishopbuddy.fragments.EditProfileFragment;
import com.project.ishopbuddy.fragments.FeedFragment;
import com.project.ishopbuddy.fragments.LogFragment;
import com.project.ishopbuddy.fragments.ScanFragment;
import com.project.ishopbuddy.fragments.SearchFragment;
import com.project.ishopbuddy.fragments.UserFragment;
import com.project.ishopbuddy.managers.FragmentNavigator;
import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.User;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mchen on 10/16/2016.
 */

public class MainActivity extends AppCompatActivity implements FragmentNavigator.FragmentNavigation, UserFragment.UpdateProfileInterface{

    public static String TAG = "tmp";
    private static MainActivity mInstance;

    public static final int FEED = FragmentNavigator.TAB1;
    public static final int SEARCH = FragmentNavigator.TAB2;
    public static final int SCAN = FragmentNavigator.TAB3;
    public static final int USER= FragmentNavigator.TAB4;

    public BottomBar mBottomBar;
    List<Fragment> fragments = new ArrayList<>(4);
    public FragmentNavigator fragManager;

    ScanFragment scanFragment;
    UserFragment userFragment;

    static Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.appbar);

        scanFragment = ScanFragment.newInstance();
        userFragment = UserFragment.newInstance(true);

        fragments.add(FeedFragment.newInstance());
        fragments.add(SearchFragment.newInstance());
        fragments.add(scanFragment);
        fragments.add(userFragment);

        mBottomBar = BottomBar.attach(this, savedInstanceState);
        mBottomBar.useFixedMode();
        mBottomBar.noTopOffset();
        mBottomBar.noScalingGoodness();
        mBottomBar.noResizeGoodness();
        mBottomBar.setMaxFixedTabs(5);
        mBottomBar.setItems(R.menu.toolbar_bottom);

        fragManager = new FragmentNavigator(getSupportFragmentManager(),R.id.container,fragments,mBottomBar,appBarLayout);

        mBottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.feed) {
                    fragManager.switchTab(FEED);
                }else if (menuItemId == R.id.search) {
                    fragManager.switchTab(SEARCH);
                }else if (menuItemId == R.id.scan) {
                    fragManager.switchTab(SCAN);
                    Intent i = new Intent(getApplicationContext(),BarcodeCaptureActivity.class);
                    startActivityForResult(i,ScanFragment.SCAN);
                }else if (menuItemId == R.id.user) {
                    fragManager.switchTab(USER);
                    SessionManager.getInstance().getUser();
                }
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.feed) {
                    fragManager.clearStack(FEED);
                }else if (menuItemId == R.id.search) {
                    fragManager.clearStack(SEARCH);
                }else if (menuItemId == R.id.scan) {
                    fragManager.switchTab(SCAN);
                }else if (menuItemId == R.id.user) {
                    fragManager.clearStack(USER);
                }
            }
        });

        mInstance = this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: " + requestCode);

        if(requestCode == AddProductFragment.REQUEST_IMAGE_CAPTURE){
            fragManager.getCurrentFragment().onActivityResult(requestCode,resultCode,data);
        }else if(requestCode == ScanFragment.SCAN){
            scanFragment.onActivityResult(requestCode,resultCode,data);
        }else if(requestCode == EditProfileFragment.REQUEST_IMAGE_CAPTURE){
            fragManager.getCurrentFragment().onActivityResult(requestCode,resultCode,data);
        }
    }

    private void initView(){

    }

    public static Toolbar getToolBar(){
        return myToolbar;
    }
    public static TextView getTitleToolBar(){
        return (TextView)myToolbar.findViewById(R.id.toolbar_title);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mBottomBar.onSaveInstanceState(outState);
    }

    public static synchronized MainActivity getInstance(){
        return mInstance;
    }

    @Override
    public void onBackPressed() {
        Boolean fragmentBool = fragManager.onBackPress();
        if (!fragmentBool) {
            super.onBackPressed();
        }
    }

    @Override
    public void onUpdateProfile(User user) {
        userFragment.updateUI(user);
    }

    class onBtnClick implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.register_button:
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void pushFragment(Fragment fragment){
        fragManager.push(fragment);
    }
}
