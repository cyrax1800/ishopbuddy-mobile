package com.project.ishopbuddy.view.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.ishopbuddy.R;

/**
 * Created by mchen on 11/27/2016.
 */

public class ReviewHolder extends RecyclerView.ViewHolder {

    public SimpleDraweeView sdvProfilePicture;
    public TextView tvUsername, tvReview;
    public RatingBar ratingBar;

    public ReviewHolder(View itemView) {
        super(itemView);
        sdvProfilePicture = (SimpleDraweeView) itemView.findViewById(R.id.iv_profile_picture);
        tvUsername = (TextView) itemView.findViewById(R.id.tv_review_username);
        tvReview = (TextView) itemView.findViewById(R.id.tv_review_user_review);
        ratingBar = (RatingBar) itemView.findViewById(R.id.ratbar_review);
    }

}
