package com.project.ishopbuddy.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.managers.FacebookAPIManager;
import com.project.ishopbuddy.managers.GoogleApiManager;
import com.project.ishopbuddy.managers.SessionManager;

/**
 * Created by mchen on 10/16/2016.
 */

public class SplashActivity extends AppCompatActivity {

    public static String TAG = "tmp";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FacebookAPIManager.getInstance().initialFacebookSDK(getApplicationContext());
        super.onCreate(savedInstanceState);

        SessionManager.getInstance().initialSession(getApplicationContext());
        String login_by = SessionManager.getInstance().getValueString(SessionManager.LOGIN_BY);
        if(login_by.equals(FacebookAPIManager.FACEBOOK) && FacebookAPIManager.getInstance().isConnected()){
            finish();
            Intent i = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(i);
//            FacebookAPIManager.getInstance().getUserProfile();
        }else if(login_by.equals(GoogleApiManager.GOOGLE)){
            GoogleApiManager.getGoogleApi().initGoogleAPI(SplashActivity.this);
            Log.d(TAG, "onCreate: " + GoogleApiManager.getGoogleApi().getGoogleApiClient().isConnected());
            if(GoogleApiManager.getGoogleApi().getGoogleApiClient().isConnected()){
                Log.d(TAG, "onCreate: ");
                silentLogin();
            }else{
                finish();
                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);
            }
        }else if(login_by.equals(APIManager.JWT)){
            finish();
            Intent i = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(i);
        }else{
            finish();
            Intent i = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(i);
        }
    }

    public void silentLogin(){
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(GoogleApiManager.getGoogleApi().getGoogleApiClient());
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: Masuk yang Activity");
        if (requestCode == GoogleApiManager.REQUEST_CODE_RESOLUTION) {
            Log.d(TAG, "onActivityResult: REQUEST_CODE_RESOLUTION");
            if (!GoogleApiManager.getGoogleApi().getGoogleApiClient().isConnecting()) {
                Log.d(TAG, "onActivityResult:  if (!AppController.getGoogleApi().getGoogleApiClient().isConnecting())");
                GoogleApiManager.getGoogleApi().connect();
            }
        }else if (requestCode == GoogleApiManager.RC_SIGN_IN) {
            Log.d(TAG, "onActivityResult: RC_SIGN_IN");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            if(!GoogleApiManager.getGoogleApi().getGoogleApiClient().isConnected()){
                GoogleApiManager.getGoogleApi().setResultAccount(result);
                Log.d(TAG, "handleSignInResult:  AppController.getGoogleApi().reconnect()");
                GoogleApiManager.getGoogleApi().reconnect();
            }
        } else {
            // Signed out, show unauthenticated UI.
            Toast.makeText(getApplicationContext(),"Login Google Fail",Toast.LENGTH_SHORT).show();
        }
    }
}
