package com.project.ishopbuddy.view.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.common.util.UriUtil;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.model.Product;
import com.project.ishopbuddy.view.holders.SmallProductHolder;

import java.util.List;

/**
 * Created by michael on 21/12/16.
 */

public class SmallProductAdapter extends RecyclerView.Adapter<SmallProductHolder> {

    public static String TAG = "tmp";

    public List<Product> _products;

    private Uri photoUri;

    public SmallProductAdapter(List<Product> products){
        photoUri = new Uri.Builder()
                .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                .path(String.valueOf(R.drawable.no_image_available))
                .build();
        this._products = products;
    }

    @Override
    public SmallProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_small_product,parent,false);
        return new SmallProductHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SmallProductHolder holder, int position) {
        Product product = _products.get(position);
        if(product.picture != null){
            Uri imageUri = Uri.parse(product.getTempUrlPic());
            holder.ivProduct.setImageURI(imageUri);
        }else{
            holder.ivProduct.setImageURI(photoUri);
        }
        holder.txtItemName.setText(product.name);
        holder.ratingBar.setRating(product.rating);
    }

    @Override
    public int getItemCount() {
        return _products.size();
    }
}
