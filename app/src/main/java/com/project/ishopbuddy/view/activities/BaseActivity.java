package com.project.ishopbuddy.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by mchen on 11/6/2016.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    public static String TAG = "tmp";

    @Override
    public void onClick(View v) {

    }
}
