package com.project.ishopbuddy.view.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.project.ishopbuddy.fragments.SearchProductFragment;
import com.project.ishopbuddy.fragments.SearchUserFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michael on 12/12/16.
 */

public class SearchPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] {"Product","User"};
    private Context context;
    public SearchProductFragment searchProductFragment;
    public SearchUserFragment searchUserFragment;

    public SearchPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            searchProductFragment = SearchProductFragment.newInstance();
            return searchProductFragment;
        }else if(position == 1){
            searchUserFragment = SearchUserFragment.newInstance();
            return searchUserFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
