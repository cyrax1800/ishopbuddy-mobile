package com.project.ishopbuddy.view.widgets;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.ishopbuddy.R;
import com.project.ishopbuddy.fragments.ProductDetailFragment;
import com.project.ishopbuddy.managers.APIManager;
import com.project.ishopbuddy.model.BaseRespond;
import com.project.ishopbuddy.model.Review;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by michael on 04/12/16.
 */

public class AddReviewDialogFragment extends DialogFragment implements View.OnClickListener {

    public String TAG = "tmp";
    RatingBar ratingBar;
    EditText etReview;
    AppCompatButton btnCanel, btnSubmit;

    Review mReview;

    float ratingValue;
    int productId;

    private ProductDetailFragment.InputReviewCallback inputReviewCallback;

    public AddReviewDialogFragment(){

    }

    public void setOnInputReviewSuccess(ProductDetailFragment.InputReviewCallback inputReviewCallback){
        this.inputReviewCallback = inputReviewCallback;
    }

    public void initView(View v){
        ratingBar = (RatingBar)v.findViewById(R.id.df_review_rating_star);
        etReview = (EditText)v.findViewById(R.id.df_et_review);
        btnCanel = (AppCompatButton)v.findViewById(R.id.df_btn_review_cancel);
        btnSubmit = (AppCompatButton)v.findViewById(R.id.df_btn_review_submit);

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if(dialog != null){
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mReview != null){
            updateUI(mReview);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_fragment_add_review,container,false);
        getDialog().setTitle("Input Review");

        initView(rootView);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ratingValue = v;
            }
        });

        btnCanel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        if(mReview != null){
            updateUI(mReview);
        }

        return rootView;
    }

    public void resetView(){
        ratingBar.setRating(3);
        etReview.setText("");
        mReview = null;
        ratingValue = 0;
    }

    public void show(FragmentManager manager, int product_id, String tag) {
        productId = product_id;
        ratingValue = 0;
        super.show(manager, tag);
    }

    public void show(FragmentManager manager, int product_id, Review review, String tag) {
        productId = product_id;
        this.mReview = review;
        ratingValue = review.rating;
        super.show(manager, tag);
    }

    private void updateUI(Review review) {
        if(ratingBar == null) return;
        ratingBar.setRating(review.rating);
        ratingValue = review.rating;
        etReview.setText(review.review);
    }

    private void sendReview(Review review){
        Call<Review> sendReview = APIManager.getReviewRepo().sendReview(APIManager.getTokenReqeust(),review);
        sendReview.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(Call<Review> call, Response<Review> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 201){
                    Review tmpReviews = response.body();
                    inputReviewCallback.addReveiewSuccess(tmpReviews);
                    resetView();
                    dismiss();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    private void updateReview(){
        Call<Review> updateReview = APIManager.getReviewRepo().editReview(APIManager.getTokenReqeust(),mReview.id,mReview);
        updateReview.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(Call<Review> call, Response<Review> response) {
                Log.d(TAG, "onResponse: Call With Code " + response.code());
                Log.d(TAG, "onResponse: Call Message " + response.message());
                if(response.code() == 200){
                    Review tmpReviews = response.body();
                    Log.d(TAG, "onResponse: " + tmpReviews.review);
                    inputReviewCallback.addReveiewSuccess(tmpReviews);
                    resetView();
                    dismiss();
                }else{
                    Gson gson = new GsonBuilder().create();
                    try {
                        BaseRespond resError = gson.fromJson(response.errorBody().string(),BaseRespond.class);
                        Log.d(TAG, "onResponse: " + resError.message);
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse: " + "error");
                    }
                    Toast.makeText(getContext(),"Something Wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    @Override
    public void dismiss() {
        resetView();
        super.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.df_btn_review_cancel:
                this.dismiss();
                break;
            case R.id.df_btn_review_submit:
                if(!validate()) {
                    Toast.makeText(getContext(),"Tidak ada boleh kosong star",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(mReview != null){
                    Log.d(TAG, "onClick: ");
                    mReview.rating = ratingValue;
                    mReview.review = etReview.getEditableText().toString();
                    updateReview();
                }else{
                    Review review = new Review();
                    review.rating = ratingValue;
                    review.product_id = productId;
                    review.review = etReview.getEditableText().toString();
                    sendReview(review);
                }

                break;
        }
    }

    private boolean validate() {
        if(ratingValue == 0) return  false;
        return true;
    }
}

