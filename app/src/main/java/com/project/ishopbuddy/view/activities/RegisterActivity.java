package com.project.ishopbuddy.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.project.ishopbuddy.R;
import com.project.ishopbuddy.managers.SessionManager;
import com.project.ishopbuddy.model.ResponsRegis;
import com.project.ishopbuddy.managers.APIManager;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    EditText etEmail,etPassword,etRepassword,etName;
    AppCompatButton btnRegister;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    String name,email,password,repassword;
    HashMap<String, String> map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initView();
        Log.d(TAG, "onResponse: Save Get " + SessionManager.getInstance().getValueString(SessionManager.KEY_TOKEN));

        btnRegister.setOnClickListener(this);

    }

    private void initView(){
        etEmail = (EditText) findViewById(R.id.email_et);
        etPassword = (EditText) findViewById(R.id.password_et);
        etRepassword = (EditText) findViewById(R.id.repassword_et);
        etName = (EditText) findViewById(R.id.name_et);
        btnRegister = (AppCompatButton) findViewById(R.id.register_button);
    }

    private void register(){
        Log.d(TAG, "register: ");
        map.put("name", name);
        map.put("email", email);
        map.put("password", password);
        Call<ResponsRegis> registerUser = APIManager.getUserRepo().register(map);
        registerUser.enqueue(new Callback<ResponsRegis>() {
            @Override
            public void onResponse(Call<ResponsRegis> call, Response<ResponsRegis> response) {
                Log.d(TAG, "onResponse: Call Success " + response.code());
                Log.d(TAG, "onResponse: Call Success " + response.message());
                Intent output = new Intent();
                output.putExtra("email", map.get("email"));
                output.putExtra("password", map.get("password"));
                setResult(LoginActivity.RESULTCODE_REGISTER,output);
                finish();
            }

            @Override
            public void onFailure(Call<ResponsRegis> call, Throwable t) {
                Log.d(TAG, "onFailure: Call Fail " + t.toString());
            }
        });
    }

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register_button:
                name = etName.getText().toString();
                email = etEmail.getText().toString();
                password = etPassword.getText().toString();
                repassword = etRepassword.getText().toString();
                if(!password.equals(repassword)){
                    Toast.makeText(getApplicationContext(),"Check your Password",Toast.LENGTH_SHORT).show();
                }else if(!validate(email)){
                    Toast.makeText(getApplicationContext(),"Email in wrong format",Toast.LENGTH_SHORT).show();
                }else{
                    register();
                }
                break;
            default:
                break;
        }
    }
}
